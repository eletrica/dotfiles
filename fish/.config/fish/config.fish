# fish config

if status is-interactive
    # Commands to run in interactive sessions can go here
end

set fish_greeting
set -x PATH $PATH $HOME/.local/bin

set LANG en_US.UTF-8
set ARCHFLAGS "-arch x86_64"
set TERM xterm-256color
set TERMINAL "alacritty"
set EDITOR "vim"
set BROWSER "brave-browser"

# Disable ctrl-s
stty -ixon

alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

alias ls='exa --icons --color=always --group-directories-first'
alias ll='exa -l --icons --color=always --group-directories-first'
alias la='exa -la --icons --color=always --group-directories-first'
alias lt='exa -aT --icons --color=always --group-directories-first'
alias vi='vim'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

starship init fish | source

set fish_color_normal brcyan
set fish_color_autosuggestion '#7d7d7d'
set fish_color_command brcyan
set fish_color_error '#ff6c6b'
set fish_color_param brcyan

# gnuplot config

# Language initialization
set locale
set encoding locale

# Default line colors and repeat cycle
set linetype 1 lc rgb "#e51e10" lw 1 # red
set linetype 2 lc rgb "#56b4e9" lw 1 # blue
set linetype 3 lc rgb "#0072b2" lw 1 # dark blue
set linetype 4 lc rgb "#0dab3c" lw 1 # dark green
set linetype 5 lc rgb "#e69f00" lw 1 # orange
set linetype 6 lc rgb "#f0e442" lw 1 # yellow
set linetype 7 lc rgb "#000000" lw 1 # black
set linetype cycle 8

# Initialize the default loadpath for shared gnuplot scripts and data.
# Please confirm that this path is correct before uncommented the line below.
# set loadpath "/usr/local/share/gnuplot/4.7/demo"

# Some commonly used functions that are not built in
# sinc(x) = sin(x)/x
# rgb(r,g,b) = sprintf("#%06x",256.*256.*255.*r+256.*255.*g+255.*b)
# hsv(h,s,v) = sprintf("#%06x",hsv2rgb(h,s,v))
# set nokey
# set key left top
# set key at 25., 5000.
# set key at graph 0.1, 0.9
# set key box
# set key opaque
# set key inside

# Other preferences
set clip two
set key outside
set key reverse

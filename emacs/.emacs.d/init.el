;; Font
(set-frame-font "MesloLGS Nerd Font 16" nil t)
;; (set-face-attribute 'default nil :font "MesloLGS Nerd Font" :height 160)

;; Add colorschemes path
(add-to-list 'custom-theme-load-path "~/.emacs.d/colorschemes/")

;; Set theme
(load-theme 'everforest_high t)

;; No init frame
(setq inhibit-startup-message t)
(setq inhibit-startup-screen t)
(setq inhibit-startup-buffer-menu t)

;; Don't show menus and others
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(tooltip-mode -1)
(auto-fill-mode t)

;; No history
(savehist-mode -1)
(setq savehist-additional-variables '(register-alist))

;; Don't use file backups
(setq backup-inhibited t)
(setq make-backup-files nil)
(setq auto-save-default nil)

(setq-default magit-save-repository-buffers 'dontask)

(defun display-startup-echo-area-message () (message ""))

;; No visible bell
(setq visible-bell nil)

(setq sentence-end-double-space nil)

;; Format frame title
(setq-default frame-title-format "%b %& emacs")

;; Break lines (wrap mode)
(global-visual-line-mode t)

;; Show line numbers
(global-display-line-numbers-mode)

;; Show column number
(column-number-mode t)

;; Highlight current line
(global-hl-line-mode t)

;; Replace when select
(delete-selection-mode t)

;; Use UTF-8 everywhere
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)

(setq use-dialog-box nil)

;; For text-mode prompts
(defalias 'yes-or-no-p 'y-or-n-p)

;; Cutting and pasting use the system clipboard
(setq select-enable-clipboard t)

;; Indentation
(setq default-tab-width 4)
(setq tab-width 4)
(setq default-fill-column 80)
(setq fill-column 80)
(setq-default evil-indent-convert-tabs nil)
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default evil-shift-round nil)

;; Add inner gaps in buffer
(set-frame-parameter nil 'internal-border-width 10)
;; (add-to-list 'default-frame-alist '(internal-border-width . 15))

;; Set fringe spacing
;; (set-fringe-mode 10)

;; Cursor
(setq-default cursor-type 'hbar)

;; Disable ctrl-z
(global-unset-key (kbd "C-z"))

;; Press esc to close panes
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; Set mouse whell scroll
(setq mouse-wheel-scroll-amount '(2 ((shift) . 1))
    mouse-wheel-progressive-speed nil
    mouse-wheel-follow-mouse 't
    scroll-step 1
)

;; Default browser
(setq browse-url-browser-function 'browse-url-generic)
(setq browse-url-generic-program "brave-browser")
(add-to-list 'browse-url-generic-args "--incognito")

;; Org Mode

;; fontify code in code blocks
(setq org-src-fontify-natively t)

(setq org-hide-leading-stars t)
;; (setq org-src-preserve-indentation -1)
;; (setq org-adapt-indentation -1)
;; (setq org-auto-align-tags nil)

;; (setq org-tags-column 0)
;; (setq org-insert-heading-respect-content t)
;; (setq org-hide-emphasis-markers t)
;; (setq org-pretty-entities t)
;; (setq electric-indent-local-mode -1)

;; (setq org-cycle-include-plain-lists 'integrate)

;; Indent mode
(setq org-startup-indented t)

;; Set heading numbers (minor mode/org-num-mode)
(setq org-startup-numerated t)

;; (setq org-indent-mode-turns-off-org-adapt-indentation t)
;; (setq org-indent-mode-turns-on-hiding-stars -1)

;; LaTEX preview
(setq org-startup-with-latex-preview nil)
(setq org-latex-create-formula-image-program 'imagemagick)
;; (setq org-latex-create-formula-image-program 'dvipng)
;; (setq org-latex-create-formula-image-program 'dvisvgm)

;; Custom keys

;; Close emacs without messages
(global-set-key (kbd "C-q") 'kill-emacs)

;; Packages
(with-eval-after-load 'package
    (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
    (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
)

;; Initialize package
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Auto-install use-package
(unless (package-installed-p 'use-package)
    (package-refresh-contents)
    (package-install 'use-package)
)

;; This is only needed once, near the top of the file
(eval-when-compile (require 'use-package))

(setq use-package-always-ensure t)
(setq use-package-always-defer t)

;; org-bullets plugin
;; (use-package org-bullets
;;   :hook (org-mode . org-bullets-mode)
;;   :config
;;   (setq org-bullets-bullet-list
;;         '("◉" "○" "○" "◎" "◎")
;;   )
;; )

;; org-modern plugin
;; (use-package org-modern)
;; (add-hook 'org-mode-hook #'org-modern-mode)

;; (setq-default cache-long-scans nil)
(setq warning-suppress-types (append warning-suppress-types '((org-element-cache))))

(use-package ess)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((R . t)))

;; Download Evil
(unless (package-installed-p 'evil)
  (package-install 'evil))

;; Enable Evil
(require 'evil)
(evil-mode 1)

;; Evil config
(setq evil-want-integration t
    evil-want-keybinding nil
    evil-vsplit-window-right t
    evil-split-window-below t
    evil-undo-system 'undo-redo)

;; Cursor
(setq evil-default-cursor (quote (t "#ffffff"))
    evil-visual-state-cursor '("#ff00ff" hbar)
    evil-normal-state-cursor '("#ffffff" hbar)
    evil-insert-state-cursor '("#00ff00" hbar)
)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(evil ess)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

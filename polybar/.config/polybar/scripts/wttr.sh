#!/usr/bin/env bash

# Vars and conditions
day=$(seq -w 0 23)
hour_day=$(date +%H)
hour_sun=$(seq -w  6 17)
hour_night=$(echo -e "$day\n$sun" | sort -n | uniq -u)
state="night"

# Verify current state
for i in $hour_sun
do
  if [[ "$i" ==  "$hour_day" ]]
  then
    state="day"
    break
  fi
done

# Set day icons
if [ $state = "day" ]
then
  clean=""
  cloud=""
  rain="歹"
  wind=""
  snow=""
else
  clean="望"
  cloud=""
  rain=""
  wind=""
  snow=""
fi

# Therm chars
term=""
celcius=""
fahrenheit="宅"

# Catch current weather from wttr
curl -Ss 'http://wttr.in/?0QTFA' > /tmp/weather
CURRENTLY=$(cat /tmp/weather | tr -dc '[:alnum:][:blank:]\n\r'| sed 's/^\ *//g' | head -1)
TEMP=$(curl -s 'https://wttr.in/?format=1' | sed -E 's/.*([0-9][0-9]).*/\1/g')

# Display correct char
case "$CURRENTLY" in
  *Sunny*|*Clear*)
    echo -n $clean
   ;;
  *Cloud*|*cloud*)
    echo -n "$cloud "
   ;;
  *Rain*|*Drizzle*|*rain*|*drizzle*)
    echo -n $rain
   ;;
  *Wind*|*wind*)
    echo -n $wind
    ;;
  *Snow*|*snow*)
    echo -n $snow
    ;;
  *) echo "";;
esac

echo " $CURRENTLY, $term $TEMP$celcius"

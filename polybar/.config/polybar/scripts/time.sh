#!/usr/bin/env bash

# Variables
t=0
clock=$(date '+%I')
day=$(date "+%Y %b %d (%a)")
hour=$(date "+%H:%M ")

# Functions
toggle() {
    t=$(((t + 1) % 2))
}

# Wait signal to toggle date format
trap "toggle" USR1

# Icons
cal=""

# Output
while true; do
  if [ $t -eq 0 ]; then
    case "$clock" in
      "00") icon="" ;;
      "01") icon="" ;;
      "02") icon="" ;;
      "03") icon="" ;;
      "04") icon="" ;;
      "05") icon="" ;;
      "06") icon="" ;;
      "07") icon="" ;;
      "08") icon="" ;;
      "09") icon="" ;;
      "10") icon="" ;;
      "11") icon="" ;;
      "12") icon="" ;;
    esac
    echo "$icon $(date "+%H:%M ")"
  else
    echo "$cal $(date "+%d/%m/%Y ")"
  fi
  sleep 1 &
  wait
done

#!/usr/bin/env bash

IP=$(hostname -i)
icon="📶"
antenna="📡"
label="直"
disconnected="睊"
connected="直"

INTERFACE=$(nmcli device | grep wireguard | awk '{print $5}')

#network=$(nmcli connection show | grep wlo1 | awk '{print $1}')
signal=$(nmcli device wifi | grep \* | awk '{print $8}')
signal_bar=$(nmcli device wifi | grep \* | awk '{print $9}' | sed 's/_/▁/g')
# signal_bar=$(nmcli device wifi | grep \* | awk '{print $9}')
signal_conn=$(/bin/cat /proc/net/wireless | grep "wlo1" | awk '{print $3}' | sed 's/\.//g')

if [[ -z $signal_conn ]]; then
   echo "%{F#ff5447}$label%{F-}"
else
    if [[ $signal_conn -ge 75 ]]; then
        color="3ce033"
    elif [[ $signal_conn -ge 50 ]]; then
        color="fce96f"
    elif [[ $signal_conn -ge 25 ]]; then
        color="ff995a"
    else
        color="ff5447"
    fi
    echo "${INTERFACE}"
fi

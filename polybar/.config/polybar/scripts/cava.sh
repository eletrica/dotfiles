#!/usr/bin/env bash

# bar="▁▂▃▄▅▆▇█"
bar="⢀⣀⣠⣤⣴⣶⣾⣿"
dict="s/;//g;"

# cava digest output with numbers separeted by semicolon,
# then, it's necessary replace these character by space.
# creating "dictionary" to replace char with bar
i=0
while [ $i -lt ${#bar} ]
do
    dict="${dict}s/$i/${bar:$i:1}/g;"
    i=$((i=i+1))
done

# create fifo
# make sure to clean pipe
pipe="/tmp/cava.fifo"
if [ -p ${pipe} ]; then
    unlink ${pipe}
fi
mkfifo ${pipe}

# write cava config
config_file="/tmp/polybar_cava_config"
echo "
[general]
bars = 15

[output]
method = raw
raw_target = $pipe
data_format = ascii
ascii_max_range = 7

" > ${config_file}

# run cava in the background
# raw target is the pipe file
cava -p ${config_file} &

# reading data from fifo
while read -r cmd; do
    echo ${cmd} | sed ${dict}
done < ${pipe}

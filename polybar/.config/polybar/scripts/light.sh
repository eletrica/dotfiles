#!/usr/bin/env bash

x="$(light -G)"
CURRENT=$(printf "%.f\n" $p $x)

# set minimum brightness to 1
light -N 1

if [ $current -ge 80 ]; then
    ICON="🌕"
elif [ $current -ge 60 ]; then
    ICON="🌔"
elif [ $current -ge 40 ]; then
    ICON="🌓"
elif [ $current -ge 20 ]; then
    ICON="🌒"
else
    ICON="🌑"
fi

echo ${ICON} ${CURRENT}%

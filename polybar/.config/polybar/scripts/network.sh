#!/usr/bin/env bash

IP=$(hostname -i)
ICON="📶"
ANTENNA="📡"
LABEL="直"
DISCONNECTED="睊"
CONNECTED="直"

#network=$(nmcli connection show | grep wlo1 | awk '{print $1}')
SIGNAL=$(nmcli device wifi | grep \* | awk '{print $8}')
SIGNAL_BAR=$(nmcli device wifi | grep \* | awk '{print $9}' | sed 's/_/▁/g')
# signal_bar=$(nmcli device wifi | grep \* | awk '{print $9}')
SIGNAL_CONN=$(/bin/cat /proc/net/wireless | grep "wlo1" | awk '{print $3}' | sed 's/\.//g')

if [[ -z ${SIGNAL_CONN} ]]; then
   echo "%{F#ff5447}$label%{F-}"
else
    if [[ ${SIGNAL_CONN} -ge 75 ]]; then
        COLOR="3ce033"
    elif [[ ${SIGNAL_CONN} -ge 50 ]]; then
        COLOR="fce96f"
    elif [[ ${SIGNAL_CONN} -ge 25 ]]; then
        COLOR="ff995a"
    else
        COLOR="ff5447"
    fi
    echo "%{F#${COLOR}}${SIGNAL_BAR}%{F-} ${SIGNAL_CONN}%"
fi

#!/usr/bin/env bash

ICON_PLAY="🎧"
ICON_PAUSE=""

DATA_PLAY=$(playerctl --player=playerctld metadata --format "{{ artist }} - {{ title }}")
DATA_CHAR=$(echo ${DATA_PLAY} | wc -c)
LIM_CHAR=75
STATUS=$(playerctl --player=playerctld status)

awk_filter="awk '{print substr($0, 1, 75)}'"

if [ "${STATUS}" = "Stopped" ]; then
    echo "No playing"
elif [ "${STATUS}" = "Paused" ]; then
    echo "${ICON_PAUSE} Paused"
elif [ "${STATUS}" = "Playing" ]; then
    `dirname $0`/scroll.py --before-text '🎵' --delay 0.3 -l 25 \
        --match-command 'playerctl --player=playerctld status' \
        --match-text 'Playing' '--scroll 1' \
        --match-text 'Paused' '--scroll 0' \
        --match-text 'Stopped' '--scroll 0' \
        --update-check true 'playerctl --player=playerctld metadata --format "{{ artist }} - {{ title }}"'
    wait
else
    echo ''
fi

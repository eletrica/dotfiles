#!/usr/bin/env bash

UPDATES=$(dnf updateinfo -q --list | wc -l)

if [ "${UPDATES}" -gt 0 ]; then
    echo " ${UPDATES}"
else
    echo " 0"
fi

#!/usr/bin/env bash

PAUSED_STATUS=$(mpc status | grep paused)
INFO=$(mpc -f '%title% - %artist%' current)

# To use in polybar hook
# polybar-msg hook playerstatus 3 >> /tmp/polybar_2
# polybar-msg hook playerstatus 2 >> /tmp/polybar_2

if [ "${SPAUSED_STATUSTATUS}" ]; then
    echo "No playing"
else
    echo "${INFO}"
fi

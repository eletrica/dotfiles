#!/usr/bin/env bash

# Variables
STATUS=$(playerctl metadata --format "{{ status }}")
STYLUS="square"
COLOR=%{F#e6d4a3}

t=0
n=0
x=$(seq 1 5)

toggle() {
  t=$(((t + 1) % 2))
}

change() {
  n=$(((n + 1) ))
  if [ $n -gt 3 ]; then n=0; fi
  case $n in
    0) COLOR=%{F#689D6A} ;;
    1) COLOR=%{F#98971A} ;;
    2) COLOR=%{F#458488} ;;
    3) COLOR=%{F#e6d4a3} ;;
esac
}

trap "toggle" USR1
trap "change" USR2

while true; do
  declare -a arr=("")
    # bar stylus
    if [ ${STYLUS} = "point" ]
      then
        icon0="⢀"
        icon1="⣀"
        icon2="⣠"
        icon3="⣤"
        icon4="⣴"
        icon5="⣶"
        icon6="⣾"
        icon7="⣿"
        icon8="⣶"
        icon9="⣤"
        icon10="⣤"
    elif [ ${STYLUS} = "square" ]
      then
        icon0="▁"
        icon1="▂"
        icon2="▃"
        icon3="▄"
        icon4="▅"
        icon5="▆"
        icon6="▇"
        icon7="█"
        icon8="█"
        icon9="_"
        icon10=" "
    fi

  # Set bar if music is playing
  STATUS=$(playerctl metadata --format "{{ status }}")
  if [[ ${STATUS} = "Playing" ]]
  then
    for i in $x
    do
      VALUE=$(( RANDOM % 10 ))
      case "${VALUE}" in
        "0") arr=("${arr[@]}" "$icon0") ;;
        "1") arr=("${arr[@]}" "$icon1") ;;
        "2") arr=("${arr[@]}" "$icon2") ;;
        "3") arr=("${arr[@]}" "$icon3") ;;
        "4") arr=("${arr[@]}" "$icon4") ;;
        "5") arr=("${arr[@]}" "$icon5") ;;
        "6") arr=("${arr[@]}" "$icon6") ;;
        "7") arr=("${arr[@]}" "$icon7") ;;
        "8") arr=("${arr[@]}" "$icon8") ;;
        "9") arr=("${arr[@]}" "$icon9") ;;
        "10") arr=("${arr[@]}" "$icon10") ;;
      esac
    done

    if [ $t -eq 0 ]; then
      STYLUS="square"
    else
      STYLUS="point"
    fi
      echo -e "${COLOR}${arr[@]}"
    sleep 1 &
    wait
  else
    echo ""
  fi
done

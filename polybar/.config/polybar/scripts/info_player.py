#!/usr/bin/env python

import re
import os
import dbus
import datetime
import argparse

# argparse config
parser = argparse.ArgumentParser(description='Control audio media with dbus module.')
parser.add_argument(
    '-c',
    '--command',
    type=str,
    choices=['play', 'pause', 'toggle', 'info'],
    metavar='command',
    dest='command',
    help='Choose a player command'
)
parser.add_argument(
    '-l',
    '--length',
    type=int,
    default='35',
    metavar='length',
    dest='length',
    help='Choose truncate length value'
)
parser.add_argument(
    '-t',
    '--time',
    type=str,
    choices=['true', 'false'],
    default='false',
    dest='time',
    help='Show current time'
)
args = parser.parse_args()

# using dbus module to get audio info from playerctld
system_bus = dbus.SessionBus()
bus_data = ("org.mpris.MediaPlayer2.playerctld", "/org/mpris/MediaPlayer2")
player = system_bus.get_object(*bus_data)
interface = dbus.Interface(player, "org.freedesktop.DBus.Properties")
metadata = interface.Get("org.mpris.MediaPlayer2.Player", "Metadata")
# position = interface.Get("org.mpris.MediaPlayer2.Player", "Position")
status = interface.Get("org.mpris.MediaPlayer2.Player", "PlaybackStatus")

# extract values from metadata
artist = str(metadata['xesam:artist'][0])
title = str(metadata['xesam:title'])
# player_lenght = metadata['mpris:length']
# total_time = str(datetime.timedelta(microseconds=player_lenght))
# current_time = str(datetime.timedelta(microseconds=position))
# time_norm = re.sub('(^.*)(\..*$)','\\1',current_time)

def stop():
    dbus.Interface(player,"org.mpris.MediaPlayer2.Player").Stop()
def play():
    dbus.Interface(player,"org.mpris.MediaPlayer2.Player").Play()
def pause():
    dbus.Interface(player,"org.mpris.MediaPlayer2.Player").Pause()
def toggle_play():
    dbus.Interface(player,"org.mpris.MediaPlayer2.Player").PlayPause()

def trunc_len(t_text, t_len):
    l = len(t_text)

    if l > t_len:
        t_out = re.sub('(^.*)( .*)','\\1',t_text[0:t_len])
        return t_out
    else:
        return t_text

def get_status(status_ctrl):
    if status_ctrl == "Playing":
        if args.time is not None and args.time == 'true':
            # print(f'{artist} - {norm_title} | {time_norm}')
            print(f'{artist} - {norm_title}')
        else:
            print(f'{artist} - {norm_title}')
    elif status_ctrl == "Paused":
        print('Paused')
    else:
        print('')

def main():
    if args.command is not None:
        command = args.command
        if command == "play":
            play()
        elif command == "pause":
            pause()
        elif command == "toggle":
            toggle_play()
        elif command == "info":
            get_status(status)
        else:
            print("No option")
    else:
        print('No valid option')

trunc = args.length

# title name is truncate
norm_title = trunc_len(title,trunc)

if __name__ == '__main__':
    main()

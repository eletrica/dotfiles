#!/usr/bin/env bash

# see man scroll.py for documentation of the following parameters

while true; do
    `dirname $0`/scroll.py --before-text "🎵" --delay 0.3 -l 25 \
            --match-command "`dirname $0`/get_status.sh --status" \
            --match-text "Playing" "--scroll 1" \
            --match-text "Paused" "--scroll 0" \
            --update-check true "`dirname $0`/get_status.sh" &
    wait
done

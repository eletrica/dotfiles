#!/usr/bin/env bash

OPT=$1

NET_INTERFACE=$(nmcli device | grep wireguard | awk '{print $5}')
VPN_ICON="󰒄"

mullvad_status(){
  COLOR="3ce033"
  RELAY=$(mullvad status | awk '{print $3}')
  if [[ ${RELAY} = "" ]]; then
    echo ""
  else
    echo %{F#${COLOR}}${VPN_ICON}%{F-} ${RELAY}
  fi
}

case ${OPT} in
  status)
    mullvad_status
  ;;
  *)
    echo ${NET_INTERFACE}
  ;;
esac

#!/bin/bash

polybar-msg cmd quit
killall cava

cmd(){
    echo $(egrep -w "$1" ~/.config/polybar/bars/$DESKTOP_SESSION.ini | sed 's/\(\[bar\/\)\(.*\)\(\]\)/polybar \2 \&/g')
}

case $DESKTOP_SESSION in
    gnome)
        bars="gnome_top"
        echo "Start polybar in gnome..."
        echo "---" | tee -a /tmp/gnome-polybar.log
        eval $(cmd $bars)
        # polybar gnome_top 2>&1 | tee -a /tmp/gnome_main.log & disown
    ;;
    awesome)
        bars="awesome_top"
        echo "Start polybar in awesome..."
        echo "---" | tee -a /tmp/awesome.log
        eval $(cmd $bars)
        # polybar gnome_top 2>&1 | tee -a /tmp/gnome_main.log & disown
    ;;
    sway)
        bars="sway_top"
        echo "Start polybar in sway..."
        echo "---" | tee -a /tmp/sway.log
        eval $(cmd $bars)
        # polybar gnome_top 2>&1 | tee -a /tmp/gnome_main.log & disown
    ;;
    *)
        echo "No polybar set to $DESKTOP_SESSION environment"
        notify-send "No polybar set to $DESKTOP_SESSION environment"
    ;;
esac
    
echo "Bars launched..."

#!/usr/bin/env bash

OPT=$(printf "Poweroff\nReboot\nLogout" | wofi -x 1685 -b -I -H 170 -W 200 --dmenu)

case "${OPT}" in
    "Poweroff")
        zenity --question --ok-label="Shutdown" --cancel-label="Cancel" --title="Poweroff System?" --text="Are you sure?" 2> /dev/null
        if [[ $? -eq 0 ]]; then
            poweroff
        fi
    ;;
    "Reboot")
        zenity --question --ok-label="Reboot" --cancel-label="Cancel" --title="Reboot System?" --text="Are you sure?" 2> /dev/null
        if [[ $? -eq 0 ]]; then
            reboot
        fi
    ;;
    "Logout")
        zenity --question --ok-label="Logout" --cancel-label="Cancel" --title="Logout Session?" --text="Are you sure?" 2> /dev/null
        if [[ $? -eq 0 ]]; then
            hyprctl dispatch exit
        fi
    ;;
esac

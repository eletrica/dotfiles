#!/usr/bin/env bash

# ==============================================================================
# Program:       term-editor
# Description:   Open terminal base editors. Used to open files in preferred
#                terminal based from desktop entry.
# Software/Tool: shell
# ==============================================================================

# source system info variables.
source /etc/os-release

# export terminal variables.
export COLORTERM=truecolor
export MICRO_TRUECOLOR=1

# script params.
APP_NAME="${1}"
FILE_PATH="${2}"

# variables.
TERMINAL=kitty
TOOLBOX_NAME=dev

FILENAME=`basename "${FILE_PATH}"`
WORK_DIR=`dirname "${FILE_PATH}"`
CUSTOM_NAME=$(echo ${WORK_DIR} | sed "s;${HOME};~;")

# verify silverblue distro
if [[ "${VARIANT_ID}" = "silverblue" ]]; then
    APP_BIN="toolbox run -c ${TOOLBOX_NAME} ${APP_NAME}"
else
    APP_BIN="$(which ${APP_NAME})"
fi

if [[ ${APP_NAME} == "nvim" ]]; then
    KITTY_CONF="${HOME}/.config/kitty/sources/neovim.conf"
else
    KITTY_CONF="${HOME}/.config/kitty/sources/text-editor.conf"
fi

# open file in one of the listed terminal.
case ${TERMINAL} in
    foot)
        foot -D "${WORK_DIR}" -w 1200x800 -T "${APP_NAME} ${CUSTOM_NAME}" "${APP_BIN}" "${FILE_PATH}"
    ;;
    gnome-terminal)
        gnome-terminal --working-directory "${WORK_DIR}" --geometry=120x30 -q --hide-menubar --title="${APP_NAME} ${CUSTOM_NAME}" -- "${APP_BIN}" "${FILENAME}"
    ;;
    kitty)
        kitty --class kitty-text-editor -T "📘 ${CUSTOM_NAME}" -c "${KITTY_CONF}" "${APP_BIN}" "${FILE_PATH}" 2> /dev/null
    ;;
    tilix)
        tilix --new-process --geometry=92x28 -w "${WORK_DIR}" -a app-new-window -t "${APP_NAME} ${CUSTOM_NAME}" -e "${APP_BIN}" "${FILE_PATH}"
    ;;
    *)
        echo "${TERMINAL} command not found."
    ;;
esac

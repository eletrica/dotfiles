#!/bin/bash

# ==============================================================================
# Program:       custom-file
# Description:   Open custom file in text-editor
# Software/Tool: micro
# ==============================================================================

DRAFTS_FOLDER="${HOME}/Storage/tmp/drafts"
FILENAME="$(date +'%Y%m%d-%k%M%S')_note"
EXT="md"

cd "${TMP_FOLDER}"
${HOME}/.local/bin/term-editor.sh micro "${DRAFTS_FOLDER}"/"${FILENAME}"."${EXT}"

#!/usr/bin/env python

# ==============================================================================
# Program:       music_db
# Description:   Set audio metadata in sqlite database
# Software/Tool: python3.10.0/toml/sqlite3
# ==============================================================================

import os
import re
import argparse
import toml
import sqlite3

# constants
AUDIO_DIR= f'{os.environ.get('HOME')}/Storage/misc/media/audio'
METADATA_DIR= f'{AUDIO_DIR}/metadata'
DATABASE = f'{os.environ.get('HOME')}/Storage/misc/media/audio/database/musics.db'


# args
parser = argparse.ArgumentParser(description='Set music tags')
parser.add_argument(
    '-d',
    '--data',
    type=str,
    default='info.toml',
    metavar='data',
    dest='f_data',
    help='Choose toml data file',
)
parser.add_argument(
    '-f',
    '--folder',
    type=str,
    default='',
    metavar='folder',
    dest='f_folder',
    help='Choose folder data file',
)
parser.add_argument(
    '--save',
    type=str,
    metavar='save',
    dest='save',
    help='Save in sqlite database',
)
parser.add_argument(
    '-t',
    '--table',
    type=str,
    default='album',
    choices=['album', 'tracks', 'covers'],
    metavar='table',
    dest='table_type',
    help='table_type',
)
args = parser.parse_args()


def insert_db(ins) -> None:
    conn = sqlite3.connect(DATABASE)
    cur = conn.cursor()
    cur.execute("pragma synchronous = off;")
    cur.execute("pragma journal_mode = off;")
    cur.execute(ins)
    conn.commit()
    conn.close()


def check_record(ins) -> int:
    conn = sqlite3.connect(DATABASE)
    cur = conn.cursor()
    res = cur.execute(ins)
    q =res.fetchone()

    if q is not None:
        out = q[0]
    else:
        out = 0

    conn.close()

    return out


def save_album(data_path) -> None:
    toml_data = toml.load(f'{data_path}/{f_data}')
    album_artist = toml_data['stats']['artist'].replace("'", "''")
    album = toml_data['stats']['album'].replace("'", "''")
    format = toml_data['stats']['format']
    gsid = toml_data['stats']['gsid']
    mbid = toml_data['stats']['mbid']
    disc_num = toml_data['stats']['discs'].split(',')
    year = toml_data['stats']['year']
    genre = toml_data['stats']['genre']
    style = toml_data['stats']['style']
    track_list = toml_data['tracks']
    n_tracks = len(toml_data['tracks'])
    image = 'cover.jpg'

    if year == "":
        year = "NULL"

    if len(disc_num) > 1:
        discs = disc_num[1]
        disc = disc_num[0]
    else:
        discs = disc_num[0]
        disc = disc_num[0]

    ins_query = (
              "select album_id from albums "
              "where "
              f"artist_id == (select artist_id from artists where name like '{album_artist}') and "
              f"title like '{album}' and "
              f"format like '{format}' and "
              f"disc == {disc} and "
              f"discs == {discs} and "
              f"ntracks == {n_tracks} and "
              f"year == {year} and "
              f"genre like '{genre}' and "
              f"style like '{style}' and "
              f"gs_id like '{gsid}' and "
              f"mb_id like '{mbid}' "
            )

    current_id = check_record(ins_query)

    if current_id > 0:
        print("This record already exists")
        print(f'ID: {current_id}')
    else:
        ins_sql = (
                "insert into albums (artist_id, title, format, disc, discs, ntracks, year, genre, style, gs_id, mb_id) "
                "values ( "
                    f"(select artist_id from artists where name like '{album_artist}'), "
                    f"'{album}', "
                    f"'{format}', "
                    f"{disc}, "
                    f"{discs}, "
                    f"{n_tracks}, "
                    f"{year}, "
                    f"'{genre}', "
                    f"'{style}', "
                    f"'{gsid}', "
                    f"'{mbid}' "
                ");")

        insert_db(ins_sql)


def save_tracks(data_path) -> None:
    toml_data = toml.load(f'{data_path}/{f_data}')
    album = toml_data['stats']['album'].replace("'", "''")
    album_artist = toml_data['stats']['artist'].replace("'", "''")
    format = toml_data['stats']['format']
    artist = toml_data['stats']['artist'].replace("'", "''")
    disc_num = toml_data['stats']['discs'].split(',')[0]
    year = toml_data['stats']['year']
    track_list = toml_data['tracks']
    n_tracks = len(track_list)

    if isinstance(track_list, list):
        idx = 0
    else:
        idx = 1

    for i in track_list:
        if isinstance(track_list, list):
            artist = track_list[idx]['artist'].replace("'", "''")
            title = track_list[idx]['title'].replace("'", "''")
            tracknumber = track_list[idx]['tracknumber']
        else:
            title = track_list[i].replace("'", "''")
            tracknumber = i.replace('track', '')

        ins_query = (
                  "select track_id from tracks "
                  "where "
                  f"album_id == (select album_id from albums where title like '{album}' and format like '{format}' and year like '{year}' and ntracks like '{n_tracks}') and "
                  f"artist_id == (select artist_id from artists where name like '{album_artist}') and "
                  f"artist like '{artist}' and "
                  f"title like '{title}' and "
                  f"tracknumber == {tracknumber} and "
                  f"disc == {disc_num} "
                )

        current_id = check_record(ins_query)

        if current_id > 0:
            print("This record already exists")
            print(f'ID: {current_id}')
        else:
            ins_sql = (
                    "insert into tracks (album_id, artist_id, artist, title, tracknumber, disc) "
                    "values ( "
                        f"(select album_id from albums where title like '{album}'), "
                        f"(select artist_id from artists where name like '{album_artist}'), "
                        f"'{artist}', "
                        f"'{title}', "
                        f"{tracknumber}, "
                        f"{disc_num} "
                    ");")

            insert_db(ins_sql)
        idx = idx + 1


# define f_data from given arg
if args.f_data is not None:
    f_data = args.f_data


def main() -> None:
    """main"""
    pwd = os.getcwd()
    data_test = os.path.isfile(f'{pwd}/{f_data}')

    if data_test:
        if args.table_type == 'album':
            save_album(pwd)
        elif args.table_type == 'tracks':
            save_tracks(pwd)
        else:
            print("Invalid option")
    else:
        data_test = os.path.isfile(f'{args.f_folder}/{f_data}')
        if data_test:
            if args.table_type == 'album':
                save_album(args.f_folder)
            elif args.table_type == 'tracks':
                save_tracks(args.f_folder)
            else:
                print("Invalid option")
        else:
            if args.f_folder == 'mpd':
                dir = os.popen("echo ${HOME}/Storage/misc/media/audio/$(dirname \"`mpc --format '%file%' current`\")").read()
                toml_d = re.sub('archived|musics', 'metadata', dir).replace('\n', '')
                save_album(toml_d)
                save_tracks(toml_d)
            else:
                print("No toml data file in metadata directory.")

            # save_album(dir)
            # save_tracks(dir)


# run only in self, not in module
if __name__ == '__main__':
    main()

#!/usr/bin/env python

# ==============================================================================
# Program:       get_comment.py
# Description:   Get comment text from mp3 tag
# Software/Tool: python3.10.0/eyed3/toml
# ==============================================================================

import os
import argparse
import re

import toml
import eyed3


parser = argparse.ArgumentParser(description='Get music tag comments')
parser.add_argument(
    '-f',
    '--file',
    type=str,
    default=None,
    metavar='filename',
    dest='filename',
    help='Choose mp3 file',
)
args = parser.parse_args()


def main() -> None:
    """main"""
    filename = args.filename
    try:
        eyed3.load(filename)
    except IOError as err:
        print(f'{err}')
    else:
        if args.filename is not None:
            tag_file = eyed3.load(filename)
            comment = tag_file.tag.comments[0].text
            print(comment)
        else:
            print('cover.jpg')


# run only in self, not in module
if __name__ == '__main__':
    main()

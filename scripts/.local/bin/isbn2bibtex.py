#!/usr/bin/env python

# ==============================================================================
# Program:       ISBN to bibtex
# Description:   Generate bibtex reference from isbn book
# Software/Tool: python
# ==============================================================================
"""
Usage: python isbn2bibtex ISBN
Pass isbn to get bibtex format output
"""

import sys
import json
import requests

# get isbn from first script argument
isbn = sys.argv[1]
url = f"https://www.googleapis.com/books/v1/volumes?q=isbn:{isbn}"
item_2_get = ["title", "subtitle", "authors", "publisher", "publishedDate"]
item_2_bib = ["title", "subtitle", "author", "publisher", "year"]
BIB_FILE = "references.bib"

print(isbn)
# request url data
r = requests.get(url)

# check status request to get data
if r.status_code == 200:
    book_info = json.loads(r.content)
    n_items = book_info["totalItems"]

    if n_items > 0:
        base = book_info['items'][0]['volumeInfo']

        with open(BIB_FILE, 'a', encoding='utf-8') as f:
            f.write('\n@book{{key,')

        for i, j in zip(item_2_get, item_2_bib):
            try:
                if i == 'authors':
                    # change comma to and in authors field
                    KEY_VALUE = ' and '.join(map(str, base[i]))
                else:
                    KEY_VALUE = base[i]
            except (KeyError, AttributeError):
                KEY_VALUE = ''

            with open(BIB_FILE, 'a', encoding='utf-8') as f:
                f.write(f'\n\t{j} = {{{KEY_VALUE}}},')

        try:
            ADDR_VALUE = book_info['items'][0]['accessInfo']['country']
        except (KeyError, AttributeError):
            ADDR_VALUE = ''

        with open(BIB_FILE, 'a', encoding='utf-8') as f:
            f.write(f'\n\taddress = {{{ADDR_VALUE}}}\n}}')

    else:
        print('No items to show')

else:
    print(r.raise_for_status())

#!/usr/bin/env bash

# ==============================================================================
# Program/File:  app_float
# Description:   Enable float mode in a sway window
# Software/Tool: sway
# ==============================================================================

$@ &
PID=$!

swaymsg -t subscribe -m '[ "window" ]' \
  | jq --unbuffered --argjson pid "${PID}" '.container | select(.pid == ${PID}) | .id' \
  | xargs -I '@' -- swaymsg '[ con_id=@ ] floating enable' &

SUBSCRIPTION=$!

echo Going into wait state

# Wait for our process to close
tail --pid=${PID} -f /dev/null

echo Killing subscription
kill ${SUBSCRIPTION}

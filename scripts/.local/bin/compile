#!/bin/bash

# ==============================================================================
# Program:       Compile
# Description:   Compile file from some extensions with specific tools.
# Software/Tool: Bash script
# ==============================================================================

current_dir=$(pwd)
quarto_bin=$(which quarto)

clean_tex(){
    rm -f *.acn *.acr *.alg *.aux *.bbl *.bcf *.bdx *.blg *.dvi *.glg *.glo *.gls *.idx *.ilg *.ind *.ist *.loa *.lof *.log *.loq *.lot *.lsg *.maf *.mtc* *.nav *.nlo *.nls *.not *.ntn *.out *.snm *.tmp *.toc *.vrb *.xdy *.xml *~
}

make_tex(){
    pdflatex "$1"
    if grep '^\\addbibresource' $1; then
        biber "$2";
    fi
    pdflatex "$1"
}

find_make(){
    if [[ -f "${current_dir}/Makefile" ]];
    then
        make
    else
        cd ..
        make
    fi
}

if [[ ! $1 ]];
then
    make
else
    if [[ "${current_dir}" =~ ^.*\.X$ ]];
    then
        pic=$(echo $current_dir | awk -F "/" '{print $9}')
        hex_file=$(ls ${current_dir}/dist/default/production/*.hex)
        mem_info=$(ls ${current_dir}/dist/default/production/*.mum)
        cmd="$1"
        case "${cmd}" in
            rec)
                make -f Makefile
                pk2cmd -ppic"${pic}" -f"${hex_file}" -m -r
                ;;
            info)
                cat "${mem_info}"
                ;;
            *)
                make -f Makefile
                ;;
        esac
    else
        file=$(readlink -f "$1")
        dir=$(dirname "${file}")
        base="${file%.*}"
        ext="${file##*.}"
        shebang=$(sed -n 1p "${file}")

        if [[ -f "${dir}/Makefile" ]]; then
            binfile=$(grep -io 'binfile = .*' Makefile | tr -d ' ' | awk -F '=' '{print $2}')
        else
            binfile="${base}"
        fi

        FG_TERM=$(echo '\033[0;33m');
        NC=$(echo '\033[0m');

        case "${ext}" in
            c|cpp|h) make compile && echo -e "${FG_TERM}"; ./"${binfile}"; echo -e "${NC}" ;;
            cir) ngspice -nb "${file}" ;;
            go) go run "${file}" ;;
            ipynb) jupyter nbconvert --to pdf "${file}" --no-prompt ;;
            jl) julia "${file}" ;;
            js|javascript) node "${file}" ;;
            lua) lua "${file}" ;;
            md) pandoc "${file}" -o ${base}.pdf;;
            ms) groff -e -ms "${file}" > "${base}".pdf ;;
            pl|perl) perl "${file}" ;;
            py|python) python "${file}" ;;
            qmd|Qmd) $quarto_bin render "${file}" --quiet ;;
            r|R) Rscript "${file}" ;;
            rb|ruby) ruby "${file}" ;;
            Rmd|rmd) R -e "rmarkdown::render("\'${file}\'")" && clean_tex ;;
            rs|rust) rustc "${file}" -o "${base}" && "${base}" ;;
            sh|shellscript) "${file}" ;;
            tex) make_tex "${file}" "${base}" && clean_tex ;;
            toml) cargo build && cargo run ;;
            typ) $quarto_bin typst compile "${file}" ;;
            *) echo "Unknown File" ;;
        esac
    fi
fi

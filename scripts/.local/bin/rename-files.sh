#!/usr/bin/env bash

# ==============================================================================
# Program:       rename-files
# Description:   Rename files with prename tool.
# Software/Tool: prename
# ==============================================================================

i=1
ARG=${1}

# remove track number preced by dot or dash
# if no parameter, add sequential number by file position

for FILE in *.mp3; do
    [ -e "${FILE}" ] || continue
    case ${ARG} in
        "dash")
            prename "s/^(\d\d - )(.*)$/\2/g" "${FILE}"
        ;;
        "dot")
            prename "s/^(\d\d\. )(.*)$/\2/g" "${FILE}"
        ;;
        *)
            prename "s/^(.*)$/${i} - \1/g" "${FILE}"
        ;;
    esac
    i=$((i+1))
    # printf -v i "%02d" $i
done

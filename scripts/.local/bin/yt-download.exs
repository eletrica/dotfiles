#!/usr/bin/env elixir

# Download mp3 from url list

defmodule S do

  # get filename from first argument
  def get_file(arg_file) do
    arg_file |> Enum.at(0)
  end

  # get list of each line from file content
  def run_list(path_file) do
    File.stream!(path_file)
    |> Stream.map(&String.trim_trailing/1)
    |> Enum.to_list
  end

  # download elements from list
  def download_list(elem_list) do
    elem_list |>
    Enum.map(fn x ->
      spawn(fn -> System.cmd("/usr/bin/yt-dlp", ["--no-continue", "--no-part", "-x", "--audio-quality", "0", "--audio-format", "mp3","#{x}", "-o", "~/Downloads/%(track_number)s-%(title)s.%(ext)s", "--parse-metadata", "playlist_index:%(track_number)s", "--quiet"]) end)
    end)
    # Enum.each(fn x -> IO.puts("this is a test #{x}") end)
  end

end

System.argv
|> S.get_file
|> S.run_list
|> S.download_list

IO.puts("Done")

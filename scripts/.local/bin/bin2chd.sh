#!/usr/bin/env bash

FILE=$(readlink -f "$1")
DIR=$(dirname "${FILE}")
BASE="${FILE%.*}"
EXT="${FILE##*.}"

case "${EXT}" in
    cue)
        echo "${BASE}"
        chdman createcd --input "${FILE}" --output "${BASE}.chd"
    ;;
    *)
        echo "No cue file"
    ;;
esac


#!/usr/bin/env bash

# ==============================================================================
# Program:       spc2mp3
# Description:   Convert spc files to mp3 using ffmpeg
# Software/Tool: ffmpeg
# ==============================================================================

# the output file has the same name of original file.
# a 5 seconds fade out filter it's applied.
# ffmpeg -t 00:03:00 -i "${FILENAME}" "${BASENAME}.mp3"
# ffmpeg -t 00:03:00 -i "${FILENAME}" -af "afade=t=out:st=175:d=5" "${BASENAME}.mp3"

if [ ${#} -gt 0 ]; then
    FILENAME=$(readlink -f "$1")
    BASENAME="${FILENAME%.*}"
    lf=$(espctag -L "${FILENAME}" | awk '{print $3}')
    time_filter=$(($lf - 5))

    ffmpeg -i "${FILENAME}" -af "afade=t=out:st=${time_filter}:d=5" -qscale 0 -ab 128k -ar 48000 "${BASENAME}.mp3"
else
    for FILENAME in *.spc; do
        BASENAME="${FILENAME%.*}"
        lf=$(espctag -L "${FILENAME}" | awk '{print $3}')
        time_filter=$(($lf - 5))

        ffmpeg -i "${FILENAME}" -af "afade=t=out:st=${time_filter}:d=5" -qscale 0 -ab 128k -ar 48000 "${BASENAME}.mp3"
    done
fi

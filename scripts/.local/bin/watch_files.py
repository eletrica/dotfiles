#!/usr/bin/env python

# ==============================================================================
# Program:       watchFiles.py
# Description:   Monitoring downloaded files
# Software/Tool: python/watchdog
# ==============================================================================

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

import os
import shutil
import json
import time

# set environment variables
home_dir = os.getenv('HOME')
pwd = os.getenv('PWD')

class MyHandler(FileSystemEventHandler):

    def on_created(self, event):

        file = None
        while file is None:
            try:
                file = open(event.src_path)
            except OSError:
                file = None
                print("WAITING FOR FILE TRANSFER....")
                time.sleep(3)
                continue

    def on_modified(self, event):
        for filename in os.listdir(track_folder):
            src = f'{track_folder}/{filename}'
            ext = os.path.splitext(filename)[1]
            if ext in doc_files:
                n_src = f'{doc_folder}/{filename}'
            elif ext in img_files:
                n_src = f'{img_folder}/{filename}'
            elif ext in iso_files:
                n_src = f'{iso_folder}/{filename}'
            elif ext in script_files:
                n_src = f'{script_folder}/{filename}'
            elif ext in software_files:
                n_src = f'{software_folder}/{filename}'
            else:
                n_src = f'{tmp_folder}/{filename}'
            # os.rename(src, n_src)
            shutil.move(src, n_src)

    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None  
        elif event.event_type == 'created':
            # Event is created, you can process it now
            print("Watchdog received created event - % s." % event.src_path)
        elif event.event_type == 'modified':
        # Event is modified, you can process it now
            print("Watchdog received modified event - % s." % event.src_path)

# file type
doc_files = ['.doc', '.docx', '.odt', '.pdf']
img_files = ['.jpeg', '.jpg', '.png', '.svg']
iso_files = ['.img', '.iso']
script_files = ['js', '.lua', '.py', '.rb', '.sh']
software_files = ['.deb', '.dnf', '.exe']

# folders
track_folder = f'{home_dir}/Downloads'
tmp_folder = f'{home_dir}/Storage/tmp'

doc_folder = f'{home_dir}/Storage/tmp/doc'
img_folder = f'{home_dir}/Storage/tmp/img'
iso_folder = f'{home_dir}/Storage/tmp/iso'
script_folder = f'{home_dir}/Storage/tmp/script'
software_folder = f'{home_dir}/Storage/tmp/software'

# rules
event_handler = MyHandler()
observer = Observer()
observer.schedule(event_handler, track_folder, recursive=True)
observer.start()

try:
    while True:
        time.sleep(1)
except:
    observer.stop()

observer.join()

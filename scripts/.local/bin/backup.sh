#!/usr/bin/env bash

DISTRO=$(source /etc/os-release; echo $NAME | awk '{print tolower($1)}')

 case ${DISTRO} in
     debian)
         RUN="/media"
     ;;
     fedora|*)
         RUN="/run/media"
     ;;
 esac

 DEVICE="$1"

 case ${DEVICE} in
     "storage-01")
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/user ${RUN}/${USER}/${DEVICE} --exclude .venv  --exclude venv
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/work ${RUN}/${USER}/${DEVICE} --exclude .venv  --exclude venv
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/tmp ${RUN}/${USER}/${DEVICE} --exclude .venv  --exclude venv
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/root ${RUN}/${USER}/${DEVICE} --exclude .venv --exclude venv --exclude compatdata --exclude shadercache --exclude "Proton *" --exclude "SteamLinux*"  --exclude "Steamworks Shared" --exclude "steamapps/compatdata" --exclude "steamapps/downloading" --exclude "steamapps/shadercache" --exclude "steamapps/temp"
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/misc ${RUN}/${USER}/${DEVICE} --exclude video  --exclude animes --exclude movies --exclude tvshows
     ;;
     "storage-03")
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/user/ ${RUN}/${USER}/${DEVICE}/@user --exclude .venv  --exclude venv
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/work/ ${RUN}/${USER}/${DEVICE}/@work --exclude .venv  --exclude venv
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/tmp/ ${RUN}/${USER}/${DEVICE}/@tmp --exclude .venv  --exclude venv
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/root/ ${RUN}/${USER}/${DEVICE}/@root --exclude .venv  --exclude venv --exclude compatdata --exclude shadercache --exclude "Proton *" --exclude "SteamLinux*"  --exclude "Steamworks Shared" --exclude "steamapps/compatdata" --exclude "steamapps/downloading" --exclude "steamapps/shadercache" --exclude "steamapps/temp"
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/misc/ ${RUN}/${USER}/${DEVICE}/@misc --exclude video  --exclude animes --exclude movies --exclude tvshows
     ;;
     "storage-04")
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/misc/media/audio/ ${RUN}/${USER}/${DEVICE}/@misc/media/audio
         # rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/misc/ ${RUN}/${USER}/${DEVICE}/@misc --exclude .git --exclude .venv --exclude venv
     ;;
     "desktop")
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/misc/media/audio/ ~/Backup/misc/media/audio
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/root/archives/backups/ ~/Backup/root/archives/backups
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/tmp/ ~/Backup/tmp
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/user/ ~/Backup/user
         rsync -avh --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/work/ ~/Backup/work
     ;;
     "homelab")
         rsync -azvh --inplace --no-whole-file --info=progress2 ${HOMELAB_USER}@${HOMELAB_IP}:~/backups/ ~/Storage/root/archives/backups/homelab
     ;;
     "homelab-sync")
         rsync -azvh --inplace --no-whole-file --info=progress2 ${HOME}/Storage/user/software/projects/server/systemd/workstation/homelab/ ${HOMELAB_USER}@${HOMELAB_IP}:~/.config/containers/systemd/homelab
         rsync -azvh --inplace --no-whole-file --info=progress2 ${HOME}/Storage/user/software/projects/server/homelab/ ${HOMELAB_USER}@${HOMELAB_IP}:~/homelab
     ;;
     "navlab")
         rsync -azvh --inplace --no-whole-file --info=progress2 ${NAVLAB_USER}@${NAVLAB_IP}:~/Storage/root/archives/backups/homelab/ ~/Storage/root/archives/backups/navlab
     ;;
     "remote-sync")
         rsync -azvh --inplace --no-whole-file --info=progress2 ${REMOTE_USER}@${REMOTE_IP}:${REMOTE_FOLDER}/ ${LOCAL_FOLDER}
     ;;
     "test")
         rsync -avh --dry-run --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/user/ ${RUN}/${USER}/storage-03/@user --exclude .venv  --exclude venv
         rsync -avh --dry-run --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/work/ ${RUN}/${USER}/storage-03/@work --exclude .venv  --exclude venv
         rsync -avh --dry-run --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/tmp/ ${RUN}/${USER}/storage-03/@tmp --exclude .venv  --exclude venv
         rsync -avh --dry-run --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/root/ ${RUN}/${USER}/storage-03/@root --exclude .venv  --exclude venv --exclude compatdata --exclude shadercache --exclude "Proton *" --exclude "SteamLinux*"  --exclude "Steamworks Shared" --exclude "steamapps/compatdata" --exclude "steamapps/downloading" --exclude "steamapps/shadercache" --exclude "steamapps/temp"
         rsync -avh --dry-run --inplace --no-whole-file --delete-before --info=progress2 ~/Storage/misc/ ${RUN}/${USER}/storage-03/@misc --exclude video  --exclude animes --exclude movies --exclude tvshows
     ;;
     *)
         echo -e "Device not registered.\nChoose: \n\t storage-01\n\t storage-03\n\t storage-04"
     ;;
 esac

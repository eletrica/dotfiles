# Licenses

View list in [choosealicense](https://choosealicense.com/appendix/).

## Academic Free License v3.0

[![License](https://img.shields.io/badge/License-AFL--3.0-blue)](LICENSE_AFL-3.0.md)

## Apache license 2.0

[![License](https://img.shields.io/badge/Licence-Apache--2.0-green.svg)](LICENSE_Apache-2.0.md)

## CERN Open Hardware Licence Version 2 - Strongly Reciprocal

[![License](https://img.shields.io/badge/License-CERN--OHL--S-blue)](LICENSE_CERN-OHL-S.md)

## Creative Commons Zero v1.0 Universal

[![License](https://img.shields.io/badge/License-CC0--1.0-blue)](LICENSE_CC0-1.0.md)

## Educational Community License v2.0

[![License](https://img.shields.io/badge/License-ECL--2.0-green)](LICENSE_ECL-2.0.md)

## GNU General Public License v3.0

[![License](https://img.shields.io/badge/License-GPL--3.0-green)](LICENSE_GPL-3.0.md)

## LaTeX Project Public License v1.3c

[![License](https://img.shields.io/badge/License-LPPL--1.3c-blue)](LICENSE_LPPL-1.3c.md)

## MIT License

[![License](https://img.shields.io/badge/Licence-MIT-green.svg)](LICENSE_MIT.md)

# Software

## Django

[![Django](https://img.shields.io/badge/Django-v5.0.3-blue?logo=Django)](https://docs.djangoproject.com/en/5.0/)

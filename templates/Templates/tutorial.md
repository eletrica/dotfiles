---
title: Title
subtitle: v1.0
author: Author
toc: true
lang: en
numbersections: true
dpi: 300
fontsize: 12pt
mainfont: Arial
geometry: "left=3cm,right=2cm,top=3cm,bottom=2cm"
header-includes: |
  \usepackage[left=3cm,top=3cm,right=2cm,bottom=2cm]{geometry}
  \usepackage{indentfirst}
  \usepackage{float}
  \usepackage{fancyhdr}
  \pagestyle{fancy}
  \let\origfigure\figure
  \let\endorigfigure\endfigure
  \renewenvironment{figure}[1][2] {
  \expandafter\origfigure\expandafter[H]
  } {
  \endorigfigure
  }
---

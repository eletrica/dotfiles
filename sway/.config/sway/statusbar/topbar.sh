#!/usr/bin/env bash

# icon/labels
net_lab="📶"
ip_lab="直"
date_lab="📅"
therm_lab=""
cpu_lab=""
mem_lab=""
arrow_lab=""

clock=$(date '+%I')
case "$clock" in
    "00") hour_lab="🕛" ;;
    "01") hour_lab="🕐" ;;
    "02") hour_lab="🕑" ;;
    "03") hour_lab="🕒" ;;
    "04") hour_lab="🕓" ;;
    "05") hour_lab="🕔" ;;
    "06") hour_lab="🕕" ;;
    "07") hour_lab="🕖" ;;
    "08") hour_lab="🕗" ;;
    "09") hour_lab="🕘" ;;
    "10") hour_lab="🕙" ;;
    "11") hour_lab="🕚" ;;
    "12") hour_lab="🕛" ;;
esac

# commands
ip_bar=$(hostname -I)
date_bar=$(date +'%Y-%m-%d')
hour_bar=$(date +'%H:%M:%S')
mem_bar=$(free -h | bat -p -r 2 | awk '{print $3 " " $4}')

# output in top bar
echo "$ip_lab $ip_bar" "$mem_lab $mem_bar" "$date_lab $date_bar" "$hour_lab $hour_bar"

# Dot Files

Use stow tool to create sym links.

# Examples

To make file links only.

```{bash}
stow --no-folding --restow FOLDER_NAME
```

To delete links:

```{bash}
stow -D FOLDER_NAME
```

#!/bin/bash

# ==============================================================================
# Program:       Makefile
# Description:   Makefile to manage dotfiles, softwares and configurations
# Software/Tool: Makefile
# ==============================================================================

DISTRO := $(shell uname -n)
PKG := apt install
MOUNT_DIR := $(HOME)/Storage

APT_PKGS := bat curl exa git micro wget

R_DEPENDENCIES_RPM := R R-bslib R-sass rstudio-desktop
R_DEPENDENCIES_RPM += freetype-devel libpng-devel libtiff-devel libjpeg-devel
R_DEPENDENCIES_RPM += fribidi-devel harfbuzz-devel
R_DEV := r-base r-base-dev libatlas3-base libssl-dev libclang-dev r-cran-openssl libpq5
R_PKGS := knitr tidyverse rmarkdown kableExtra devtools seewave latex2exp gsignal patchwork Metrics

stow:
	stow --no-folding calcurse cava emacs fonts foot hyprland imv kitty mako micro mpd mpv nautilus ncmpcpp nvim scripts shell starship sway templates themes vim waybar wofi x zathura

r-install:
	sudo $(PKG) $(R_DEV)

r-packages: r-install
	sudo chmod o+w /usr/local/lib/R -R
	@for pkg in $(R_PKGS); do R -e "install.packages('$$pkg')"; done

starship-install:
	curl -sS https://starship.rs/install.sh | sh

quarto-install:
	wget https://github.com/quarto-dev/quarto-cli/releases/download/v1.3.450/quarto-1.3.450-linux-amd64.tar.gz
	sudo tar -C /opt -xvzf quarto-1.3.450-linux-amd64.tar.gz
	sudo ln -s /opt/quarto-1.3.450/bin/quarto /usr/local/bin/quarto
	rm quarto-1.3.450-linux-amd64.tar.gz
	quarto check

brave-repo:
	curl https://brave-browser-rpm-release.s3.brave.com/brave-browser.repo > ~/brave-browser.repo
	sudo cp ~/brave-browser.repo /etc/yum.repos.d/brave-browser.repo
	rm ~/brave-browser.repo

brave-install:
	sudo dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/
	sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
	sudo $(PKG) brave-browser

plug-install:
	curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

nautilus-config:
	pip install --user nautilus-open-any-terminal
	nautilus -q
	glib-compile-schemas ~/.local/share/glib-2.0/schemas/

editor-themes:
	sudo cp themes/.local/share/gedit/styles/* /usr/share/gnome-text-editor/styles/ -R

app-config: stow brave-install plug-install nautilus-config editor-themes

# symlinks
# first, run sudo lsblk -f | grep sda1 | awk '{print "UUID="$4 " /mnt/ssd " $2 " defaults 0 0"}' >> /etc/fstab
links:
	ln -sf $(MOUNT_DIR)/misc/media/audio/musics $(HOME)/Music
	ln -sf $(MOUNT_DIR)/user/documents $(HOME)/Documents
	ln -sf $(MOUNT_DIR)/user/software/projects $(HOME)/Documents
	ln -sf $(MOUNT_DIR)/user/literature/books/library $(HOME)/Documents
	ln -sf $(MOUNT_DIR)/user/media/images/screenshots $(HOME)/Pictures/Screenshots

.PHONY stow r-install r-packages startship-install quarto-install \
	brave-repo brave-install plug-install nautilus-config editor-themes app-config links

--  rainbow awesome theme

local user_themes_path = "~/.config/awesome/themes/"
local dpi = require("beautiful.xresources").apply_dpi

local wallpaper_file = "wallpapers/colors-1.png"
local theme_dir = "rainbow/"

-- {{{ Main
local theme = {}
theme.wallpaper = user_themes_path .. theme_dir .. wallpaper_file
-- }}}

-- {{{ Styles

theme.font      = "MesloLGS NF Regular 10"
-- {{{ Colors
theme.fg_normal  = "#DCDCCC"
theme.fg_focus   = "#F0DFAF"
theme.fg_urgent  = "#CC9393"
theme.bg_normal  = "#3F3F3FDD"
theme.bg_focus   = "#1E2320"
theme.bg_urgent  = "#3F3F3F"
theme.bg_systray = theme.bg_normal
-- }}}

-- {{{ Borders
theme.useless_gap   = dpi(5)
theme.border_width  = dpi(2)
theme.border_normal = "#3F3F3F"
theme.border_focus  = "#6F6F6F"
theme.border_marked = "#CC9393"
-- }}}

-- {{{ Titlebars
theme.titlebar_bg_focus  = "#3F3F3F"
theme.titlebar_bg_normal = "#3F3F3FAA"
-- }}}

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- titlebar_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- Example:
--theme.taglist_bg_focus = "#CC9393"
-- }}}

-- {{{ Widgets
-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.fg_widget        = "#AECF96"
--theme.fg_center_widget = "#88A175"
--theme.fg_end_widget    = "#FF5656"
--theme.bg_widget        = "#494B4F"
--theme.border_widget    = "#3F3F3F"
-- }}}

-- {{{ Mouse finder
theme.mouse_finder_color = "#CC9393"
-- mouse_finder_[timeout|animate_timeout|radius|factor]
-- }}}

-- {{{ Menu
-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_height = dpi(15)
theme.menu_width  = dpi(100)
-- }}}

-- {{{ Icons
-- {{{ Taglist
theme.taglist_squares_sel   = user_themes_path .. theme_dir.. "/taglist/squarefz.png"
theme.taglist_squares_unsel = user_themes_path .. theme_dir.. "/taglist/squarez.png"
--theme.taglist_squares_resize = "false"
-- }}}

-- {{{ Misc
theme.awesome_icon           = user_themes_path .. theme_dir.. "/icons/awesome-icon.png"
theme.tux_icon               = user_themes_path .. theme_dir.. "/icons/tux.svg"
theme.redhat_icon            = user_themes_path .. theme_dir.. "/icons/redhat.svg"
theme.fedora_icon            = user_themes_path .. theme_dir.. "/icons/fedora.svg"
theme.bash_icon              = user_themes_path .. theme_dir.. "/icons/bash.svg"
theme.gnu_icon               = user_themes_path .. theme_dir.. "/icons/gnu.svg"
theme.zsh_icon               = user_themes_path .. theme_dir.. "/icons/zsh.svg"
theme.micro_icon             = user_themes_path .. theme_dir.. "/icons/micro.svg"
theme.menu_submenu_icon      = user_themes_path .. theme_dir.. "/icons/submenu.png"
-- }}}

-- {{{ Layout
theme.layout_tile       = user_themes_path .. theme_dir.. "/layouts/tile.png"
theme.layout_tileleft   = user_themes_path .. theme_dir.. "/layouts/tileleft.png"
theme.layout_tilebottom = user_themes_path .. theme_dir.. "/layouts/tilebottom.png"
theme.layout_tiletop    = user_themes_path .. theme_dir.. "/layouts/tiletop.png"
theme.layout_fairv      = user_themes_path .. theme_dir.. "/layouts/fairv.png"
theme.layout_fairh      = user_themes_path .. theme_dir.. "/layouts/fairh.png"
theme.layout_spiral     = user_themes_path .. theme_dir.. "/layouts/spiral.png"
theme.layout_dwindle    = user_themes_path .. theme_dir.. "/layouts/dwindle.png"
theme.layout_max        = user_themes_path .. theme_dir.. "/layouts/max.png"
theme.layout_fullscreen = user_themes_path .. theme_dir.. "/layouts/fullscreen.png"
theme.layout_magnifier  = user_themes_path .. theme_dir.. "/layouts/magnifier.png"
theme.layout_floating   = user_themes_path .. theme_dir.. "/layouts/floating.png"
theme.layout_cornernw   = user_themes_path .. theme_dir.. "/layouts/cornernw.png"
theme.layout_cornerne   = user_themes_path .. theme_dir.. "/layouts/cornerne.png"
theme.layout_cornersw   = user_themes_path .. theme_dir.. "/layouts/cornersw.png"
theme.layout_cornerse   = user_themes_path .. theme_dir.. "/layouts/cornerse.png"
-- }}}

-- {{{ Titlebar
theme.titlebar_close_button_focus  = user_themes_path .. theme_dir.. "/titlebar/close_focus.png"
theme.titlebar_close_button_normal = user_themes_path .. theme_dir.. "/titlebar/close_normal.png"

theme.titlebar_minimize_button_normal = user_themes_path .. theme_dir.. "/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = user_themes_path .. theme_dir.. "/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_focus_active  = user_themes_path .. theme_dir.. "/titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active = user_themes_path .. theme_dir.. "/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive  = user_themes_path .. theme_dir.. "/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive = user_themes_path .. theme_dir.. "/titlebar/ontop_normal_inactive.png"

theme.titlebar_sticky_button_focus_active  = user_themes_path .. theme_dir.. "/titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active = user_themes_path .. theme_dir.. "/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive  = user_themes_path .. theme_dir.. "/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive = user_themes_path .. theme_dir.. "/titlebar/sticky_normal_inactive.png"

theme.titlebar_floating_button_focus_active  = user_themes_path .. theme_dir.. "/titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active = user_themes_path .. theme_dir.. "/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive  = user_themes_path .. theme_dir.. "/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive = user_themes_path .. theme_dir.. "/titlebar/floating_normal_inactive.png"

theme.titlebar_maximized_button_focus_active  = user_themes_path .. theme_dir.. "/titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active = user_themes_path .. theme_dir.. "/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = user_themes_path .. theme_dir.. "/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = user_themes_path .. theme_dir.. "/titlebar/maximized_normal_inactive.png"
-- }}}
-- }}}

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80

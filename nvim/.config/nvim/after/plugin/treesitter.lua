return {
    require 'nvim-treesitter.configs'.setup {
        ensure_installed = {
            "bash",
            "eex",
            "elixir",
            "julia",
            "latex",
            "lua",
            "make",
            "markdown",
            "markdown_inline",
            "python",
            "r",
            "regex",
            "vim",
        },
        highlight = {
            enable = false,
            additional_vim_regex_highlighting = { "markdown" },
        },
    }
}

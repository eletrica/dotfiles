local colors = {
    background    =  '#001e26',
    foreground    =  '#9bc1c2',
    listchar      =  '#19343B',
    current_line  =  '#04252E',
    comment       =  '#657b83',
    color0        =  '#002731',
    color1        =  '#d01b24',
    color2        =  '#6bbe6c',
    color3        =  '#a57705',
    color4        =  '#2075c7',
    color5        =  '#c61b6e',
    color6        =  '#259185',
    color7        =  '#e9e2cb',
}

local M =  {
    normal = {
        a = {bg = colors.color4, fg = colors.background, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.color3},
    },
    insert = {
        a = {bg = colors.color2, fg = colors.background, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.color4},
    },
    visual = {
        a = {bg = colors.color3, fg = colors.background, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.color3},
    },
    replace = {
        a = {bg = colors.color1, fg = colors.color0, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color1},
        c = {bg = colors.current_line, fg = colors.foreground},
    },
    command = {
        a = {bg = colors.background, fg = colors.foreground, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.foreground},
    },
    inactive = {
        a = {bg = colors.background, fg = colors.foreground, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.comment},
        c = {bg = colors.background, fg = colors.foreground},
    },
    diff_color = {
        added = {bg = colors.listchar, fg = colors.color2},
        modified = {bg = colors.listchar, fg = colors.color3},
        removed = {bg = colors.listchar, fg = colors.color1},
    },
}

M.terminal = M.normal

return M
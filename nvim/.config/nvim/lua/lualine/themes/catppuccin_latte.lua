local colors = {
    background    =  '#eff1f5',
    foreground    =  '#4c4f69',
    listchar      =  '#acb0be',
    current_line  =  '#e6e9ef',
    comment       =  '#206431',
    color0        =  '#5c5f77',
    color1        =  '#d20f39',
    color2        =  '#40a02b',
    color3        =  '#df8e1d',
    color4        =  '#1e66f5',
    color5        =  '#8839ef',
    color6        =  '#04a5e5',
    color7        =  '#dc8a78',
}

local M =  {
    normal = {
        a = {bg = colors.color4, fg = colors.background, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.color3},
    },
    insert = {
        a = {bg = colors.color2, fg = colors.background, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.color4},
    },
    visual = {
        a = {bg = colors.color3, fg = colors.background, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.color3},
    },
    replace = {
        a = {bg = colors.color1, fg = colors.color0, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color1},
        c = {bg = colors.current_line, fg = colors.foreground},
    },
    command = {
        a = {bg = colors.background, fg = colors.foreground, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.foreground},
    },
    inactive = {
        a = {bg = colors.background, fg = colors.foreground, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.comment},
        c = {bg = colors.background, fg = colors.foreground},
    },
    diff_color = {
        added = {bg = colors.listchar, fg = colors.color2},
        modified = {bg = colors.listchar, fg = colors.color3},
        removed = {bg = colors.listchar, fg = colors.color1},
    },
}

M.terminal = M.normal

return M
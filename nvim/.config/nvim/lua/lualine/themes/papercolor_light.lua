local colors = {
    background    =  '#eeeeee',
    foreground    =  '#444444',
    listchar      =  '#D6D6D6',
    current_line  =  '#dedede',
    comment       =  '#878787',
    color0        =  '#eeeeee',
    color1        =  '#af0000',
    color2        =  '#008700',
    color3        =  '#5f8700',
    color4        =  '#0087af',
    color5        =  '#878787',
    color6        =  '#005f87',
    color7        =  '#585858',
}

local M =  {
    normal = {
        a = {bg = colors.color4, fg = colors.background, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.color3},
    },
    insert = {
        a = {bg = colors.color2, fg = colors.background, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.color4},
    },
    visual = {
        a = {bg = colors.color3, fg = colors.background, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.color3},
    },
    replace = {
        a = {bg = colors.color1, fg = colors.color0, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color1},
        c = {bg = colors.current_line, fg = colors.foreground},
    },
    command = {
        a = {bg = colors.background, fg = colors.foreground, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.foreground},
    },
    inactive = {
        a = {bg = colors.background, fg = colors.foreground, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.comment},
        c = {bg = colors.background, fg = colors.foreground},
    },
    diff_color = {
        added = {bg = colors.listchar, fg = colors.color2},
        modified = {bg = colors.listchar, fg = colors.color3},
        removed = {bg = colors.listchar, fg = colors.color1},
    },
}

M.terminal = M.normal

return M
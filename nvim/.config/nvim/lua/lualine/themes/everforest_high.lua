local colors = {
    background    =  '#20272C',
    foreground    =  '#d8caac',
    listchar      =  '#363C41',
    current_line  =  '#2D3338',
    comment       =  '#5D6259',
    color0        =  '#3c474d',
    color1        =  '#e68183',
    color2        =  '#a7c080',
    color3        =  '#d9bb80',
    color4        =  '#83b6af',
    color5        =  '#d39bb6',
    color6        =  '#87c095',
    color7        =  '#a7c080',
}

local M =  {
    normal = {
        a = {bg = colors.color4, fg = colors.background, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.color3},
    },
    insert = {
        a = {bg = colors.color2, fg = colors.background, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.color4},
    },
    visual = {
        a = {bg = colors.color3, fg = colors.background, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.color3},
    },
    replace = {
        a = {bg = colors.color1, fg = colors.color0, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color1},
        c = {bg = colors.current_line, fg = colors.foreground},
    },
    command = {
        a = {bg = colors.background, fg = colors.foreground, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.foreground},
    },
    inactive = {
        a = {bg = colors.background, fg = colors.foreground, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.comment},
        c = {bg = colors.background, fg = colors.foreground},
    },
    diff_color = {
        added = {bg = colors.listchar, fg = colors.color2},
        modified = {bg = colors.listchar, fg = colors.color3},
        removed = {bg = colors.listchar, fg = colors.color1},
    },
}

M.terminal = M.normal

return M
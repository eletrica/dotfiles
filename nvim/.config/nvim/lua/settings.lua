-- Settings --

local search_highlight = true

local options = {
    autoindent = true,
    autochdir = true,
    autowrite = true,
    clipboard = 'unnamedplus',   -- Copy/paste to system clipboard
    cmdheight = 0,               -- Height value when in cmdmode
    cursorline = true,
    encoding = 'utf-8',
    expandtab = true,
    foldenable = false,          -- Disable fold
    foldmethod = 'marker',
    foldmarker = '{{{,}}}',
    hidden = true,               -- Enable background buffers
    history = 100,               -- Remember N lines in history
    hlsearch = true,
    ignorecase = true,
    incsearch = true,
    lazyredraw = true,           -- Faster scrolling
    linebreak = true,
    mouse = 'a',                 -- Enable mouse support
    number = true,
    relativenumber = false,
    scrolloff = 15,
    shiftwidth = 4,
    signcolumn = 'yes',
    smartindent = true,
    softtabstop = 4,
    spell = false,
    splitright = true,
    swapfile = false,            -- Don't use swapfile
    synmaxcol = 240,             -- Max column for syntax highlight
    tabstop = 4,
    termguicolors = true,
    textwidth = 0,
    updatetime = 50,
    wrap = true,
    guifont = {"MesloLGS Nerd Font:16"},
}

for k, v in pairs(options) do
  vim.opt[k] = v
end

-- set end of buffer char
vim.wo.fillchars='eob: '

-- remove whitespace on save
vim.cmd [[au BufWritePre * :%s/\s\+$//e]]

-- retab file
-- vim.cmd("retab")

-- set listchars
vim.opt.list = true

-- set only tab in listchars
local tab_listchar = {
    tab = '→ ',
    -- tab = '│─',
}

-- set all in listchars
local all_listchars = {
    tab = '→ ',
    space = '·',
    nbsp = '␣',
    trail = '•',
    eol = '↲',
    precedes = '«',
    extends = '»',
}

-- set listchar when open new buffer or window
vim.api.nvim_create_autocmd({"BufEnter", "BufWinEnter"}, {
  callback = function()
    vim.opt.listchars = tab_listchar
  end
})

-- format options:
-- c: Auto-wrap comments using 'textwidth', inserting the current comment leader automatically.
-- r: Automatically insert the current comment leader after hitting
-- o: Automatically insert the current comment leader after hitting 'o' or 'O' in Normal mode.
-- In case comment is unwanted in a specific place use CTRL-U to quickly delete it.

vim.cmd [[au BufEnter * set fo-=c fo-=r fo-=o]]

-- open terminal in new right vertical split
vim.cmd [[command Term :botright vsplit term://$SHELL]]

vim.cmd [[
    autocmd TermOpen * setlocal listchars= nonumber norelativenumber nocursorline
    autocmd TermOpen * startinsert
    autocmd BufLeave term://* stopinsert
    hi CursorLine ctermbg=237 ctermfg=grey guibg=#3a3a3a cterm=none gui=none
    hi CursorLineNr ctermfg=white ctermbg=magenta guibg=#3a3a3a cterm=none gui=none
    set listchars=tab:→\ ,space:·,nbsp:␣,trail:•,eol:¶,precedes:«,extends:»
]]

vim.api.nvim_exec([[
  augroup YankHighlight
    autocmd!
    autocmd TextYankPost * silent! lua vim.highlight.on_yank{higroup="IncSearch", timeout=200}
  augroup end
]], false)

-- disable nvim intro
vim.opt.shortmess:append "sI"

-- enable nvim-tree lua
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- guicursor style
vim.opt.guicursor = "n-v-c-sm:hor20,i-ci-ve:ver25,r-cr-o:hor20"

-- custom filetype
vim.api.nvim_create_autocmd({"BufNewFile", "BufRead"}, {
    pattern = {"*.swayconfig", "*.sway", "*.sway.conf", "*/sway/*"},
    callback = function()
        vim.opt["filetype"] = "swayconfig"
        vim.cmd("TSEnable highlight")
    end
})

vim.api.nvim_create_autocmd({"BufNewFile", "BufRead"}, {
    pattern = {"*.hypr", "*.hyprlang", "hyprland.conf", "*/hypr/*"},
    callback = function()
        vim.opt["filetype"] = "hyprlang"
        vim.cmd("TSEnable highlight")
        vim.bo.commentstring = "# %s"
    end
})

-- add commentstring option
vim.api.nvim_create_autocmd("FileType", {
    pattern = {"sql"},
    callback = function()
        vim.bo.commentstring = "-- %s"
    end
})

-- no highlight after search
if search_highlight == false then
    vim.cmd([[
    augroup incsearch-highlight
      autocmd!
      autocmd CmdlineEnter /,\? :set hlsearch
      autocmd CmdlineLeave /,\? :set nohlsearch
    augroup END
    ]], false)
end


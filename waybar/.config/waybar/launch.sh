#!/usr/bin/env bash

OPTION=$1

STYLE="default"
CONFIG_FILE="config"
WAYBAR_DIR="$HOME/.config/waybar"

stop_all(){
    killall waybar > /dev/null 2>&1
    # killall cava > /dev/null 2>&1
    # killall status_player./sh > /dev/null 2>&1
}

start(){
    if [[ ${DESKTOP_SESSION} == "hyprland" ]]; then
        waybar -s "${WAYBAR_DIR}/style.css" -c "${WAYBAR_DIR}/bars/hypr.jsonc" > /dev/null 2>&1 &
    else
        echo "Unknown desktop";
    fi
}

case ${OPTION} in
    start)
        stop_all
        start
    ;;
    stop)
        stop_all
    ;;
    *)
        echo "Restarting waybar in ${DESKTOP_SESSION}"
        stop_all
        start
    ;;
esac

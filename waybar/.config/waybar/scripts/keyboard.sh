#!/usr/bin/env bash

KEY=$(hyprctl devices -j | jq -r '.keyboards[] | select(.main == true) | .active_keymap')

case ${KEY} in
    "English (US)")
        FLAG="🇺🇸"
    ;;
    "English (US, alt. intl.)")
        FLAG="🇧🇷"
    ;;
    *)
        FLAG="🇺🇸"
    ;;
esac

echo "${FLAG}"

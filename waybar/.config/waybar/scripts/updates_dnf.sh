#!/bin/sh

ICON=""
ICON_ALT="📦"
UPDATES=$(dnf updateinfo list --updates -q | wc -l)

if [ "${UPDATES}" -gt 0 ]; then
    echo "${ICON_ALT} $((${UPDATES}-1))"
else
    echo "${ICON_ALT} 0"
fi

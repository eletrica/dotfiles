#!/usr/bin/env bash

ICON_PLAY="🎧"
ICON_PAUSE=""

while true; do
    `dirname $0`/scroll.py --before-text '🎵 ' --delay 0.3 --length 25 \
        --match-command 'mpc status %state%' \
        --match-text 'playing' '--scroll 1' \
        --match-text 'paused' '--scroll 0' \
        --match-text 'stopped' '--scroll 0' \
        --update-check true 'mpc --format "%artist% - %title%" current'
    wait
done

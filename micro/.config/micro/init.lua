-- =============================================================================
-- Program:       Init lua
-- Description:   Init lua file to micro text editor
-- Software/Tool: micro/lua
-- =============================================================================

VERSION = "1.0.0"

local micro = import("micro")
local config = import("micro/config")
local shell = import("micro/shell")
local buffer = import("micro/buffer")
local home_dir = os.getenv("HOME")
local dark_mode = "gruvbox_dark"
local light_mode = "papercolor"
local current_mode = dark_mode


function init()
    -- this will modify the settings.json file
    -- commented default values
    config.SetGlobalOptionNative("autoclose", false)
    -- config.SetGlobalOptionNative("autoindent", true)
    -- config.SetGlobalOptionNative("autosave", 0)
    -- config.SetGlobalOptionNative("autosu", false)
    config.SetGlobalOptionNative("backup", false)
    -- config.SetGlobalOptionNative("backupdir", "")
    config.SetGlobalOptionNative("basename", true)
    -- config.SetGlobalOptionNative("clipboard", "external")
    -- config.SetGlobalOptionNative("colorcolumn", 0)
    config.SetGlobalOptionNative("colorscheme", dark_mode)
    -- config.SetGlobalOptionNative("comment", true)
    config.SetGlobalOptionNative("cursorline", true)
    -- config.SetGlobalOptionNative("diff", true)
    config.SetGlobalOptionNative("diffgutter", true)
    config.SetGlobalOptionNative("divchars", "  ")
    -- config.SetGlobalOptionNative("divreverse", true)
    -- config.SetGlobalOptionNative("encoding", "utf-8")
    config.SetGlobalOptionNative("eofnewline", false)
    -- config.SetGlobalOptionNative("fakecursor", false)
    -- config.SetGlobalOptionNative("fastdirty", false)
    config.SetGlobalOptionNative("fileformat", "unix")
    -- config.SetGlobalOptionNative("filetype", "unknown")
    config.SetGlobalOptionNative("ftoptions", false)
    config.SetGlobalOptionNative("hlsearch", true)
    -- config.SetGlobalOptionNative("ignorecase", false)
    config.SetGlobalOptionNative("incsearch", true)
    config.SetGlobalOptionNative("indentchar", "→")
    config.SetGlobalOptionNative("infobar", true)
    -- config.SetGlobalOptionNative("initlua", true)
    -- config.SetGlobalOptionNative("keepautoindent", false)
    -- config.SetGlobalOptionNative("keymenu", false)
    -- config.SetGlobalOptionNative("linter", true)
    -- config.SetGlobalOptionNative("literate", true)
    -- config.SetGlobalOptionNative("matchbrace", true)
    -- config.SetGlobalOptionNative("mkparents", false)
    -- config.SetGlobalOptionNative("mouse", true)
    -- config.SetGlobalOptionNative("multiopen", "tab")
    config.SetGlobalOptionNative("parsecursor", true)
    -- config.SetGlobalOptionNative("paste", false)
    -- config.SetGlobalOptionNative("permbackup", false)
    -- config.SetGlobalOptionNative("readonly", false)
    -- config.SetGlobalOptionNative("relativeruler", false)
    -- config.SetGlobalOptionNative("reload", "prompt")
    config.SetGlobalOptionNative("rmtrailingws", true)
    -- config.SetGlobalOptionNative("ruler", true)
    -- config.SetGlobalOptionNative("savecursor", false)
    config.SetGlobalOptionNative("savehistory", false)
    -- config.SetGlobalOptionNative("saveundo", false)
    -- config.SetGlobalOptionNative("scrollbar", false)
    -- config.SetGlobalOptionNative("scrollmargin", 3)
    -- config.SetGlobalOptionNative("scrollspeed", 2)
    -- config.SetGlobalOptionNative("smartpaste", true)
    config.SetGlobalOptionNative("softwrap", true)
    -- config.SetGlobalOptionNative("splitbottom", true)
    -- config.SetGlobalOptionNative("splitright", true)
    -- config.SetGlobalOptionNative("status", true)
    -- config.SetGlobalOptionNative("statusformatl", "$(filename) $(modified)($(line),$(col)) $(status.paste)| ft:$(opt:filetype) | $(opt:fileformat) | $(opt:encoding)")
    -- config.SetGlobalOptionNative("statusformatr", "$(bind:ToggleKeyMenu): bindings, $(bind:ToggleHelp): help")
    -- config.SetGlobalOptionNative("statusline", true)
    -- config.SetGlobalOptionNative("sucmd", "sudo")
    -- config.SetGlobalOptionNative("syntax", true)
    -- config.SetGlobalOptionNative("tabmovement", false)
    config.SetGlobalOptionNative("tabhighlight", true)
    config.SetGlobalOptionNative("tabreverse", false)
    config.SetGlobalOptionNative("tabsize", 4)
    -- config.SetGlobalOptionNative("tabstospaces", false)
    -- config.SetGlobalOptionNative("useprimary", true)
    config.SetGlobalOptionNative("wordwrap", true)
    -- config.SetGlobalOptionNative("xterm", false)

    -- this will modify the bindings.json file
    config.TryBindKey("Alt-\\", "lua:initlua.new_view", true)
    config.TryBindKey("Alt-c", "lua:initlua.toggle_colorscheme", true)
    config.TryBindKey("Alt-h", "lua:initlua.hsplit_down", true)
    config.TryBindKey("Ctrl-Alt-j", "lua:initlua.toggle_colors_down", true)
    config.TryBindKey("Ctrl-Alt-k", "lua:initlua.toggle_colors_up", true)
    config.TryBindKey("Alt-l", "lua:initlua.toggle_column", true)
    config.TryBindKey("Alt-r", "lua:initlua.toggle_relative", true)
    config.TryBindKey("Alt-t", "lua:initlua._MainTest", true)
    config.TryBindKey("Alt-v", "lua:initlua.vsplit_left", true)
    config.TryBindKey("Alt-z", "lua:initlua.toggle_softwrap", true)
    config.TryBindKey("Alt-1", "command:tabswitch 1", true)
    config.TryBindKey("Alt-2", "command:tabswitch 2", true)
    config.TryBindKey("Alt-3", "command:tabswitch 3", true)
    config.TryBindKey("Alt-4", "command:tabswitch 4", true)
    config.TryBindKey("Alt-5", "command:tabswitch 5", true)
    config.TryBindKey("Alt-Tab", "IndentLine", true)
    config.TryBindKey("Alt-Left", "PreviousTab", true)
    config.TryBindKey("Alt-Right", "NextTab", true)
    config.TryBindKey("Ctrl-b", "lua:initlua.output", true)
    config.TryBindKey("Ctrl-t", "command:run kitty --class kitty-slide", true)
    config.TryBindKey("Ctrl-Left", "StartOfLine", true)
    config.TryBindKey("Ctrl-Right", "EndOfLine", true)
    config.TryBindKey("Shift-Right", "command:tabmove +1", true)
    config.TryBindKey("Shift-Left", "command:tabmove -1", true)
    config.TryBindKey("Home", "Center", true)

    config.TryBindKey("Ctrl-l", "lua:initlua.makefile_compile", true)
    config.TryBindKey("Ctrl-p", "lua:initlua.make", true)

    -- temp, not used
    -- config.TryBindKey("Alt-s", "lua:initlua.sort_uniq", true)
    -- config.TryBindKey("Ctrl-j", "StartOfLine,CursorDown,InsertNewline,CursorUp", true)
    -- config.TryBindKey("Ctrl-k", "StartOfLine,InsertNewline,CursorUp", true)

    -- custom functions call
    -- change_colorscheme(b, false)
end


-- count buffer
local nBuffer = -1
-- change to file directory when open buffer pane
-- get bufpane object
function onBufPaneOpen(bp)
    nBuffer = nBuffer+1
    local path = import("path")
    local ft = bp.Buf:FileType()

    -- get filepath and normalize space char
    local filepath =  path.Dir(bp.Buf.Path):gsub(" ", "\\%0")

    -- change to filepath directory
    -- keep work directory from first opened buffer
    if nBuffer <= 1 then
        bp:HandleCommand("cd " .. filepath)
    end

    -- if nBuffer <= 1 and ft == "tex" then
    --     bp:HandleCommand("cd " .. filepath)
    --     TexFlag = 1
    -- elseif TexFlag == 0 then
    --     bp:HandleCommand("cd " .. filepath)
    -- end
end


-- set tabtospaces on buffer open by filetype
-- get buffer object
function onBufferOpen(buf)
    local ft = buf:FileType()
    local filename = buf:GetName()

    local isTabFile = (ft == "makefile" or
        ft == "go"  or
        ft == "unknown")

    if isTabFile then
        buf:SetOption("tabstospaces", "off")
        micro.Log("tabstospaces off")
    else
        buf:SetOption("tabstospaces", "on")
        micro.Log("tabstospaces on")
    end
end


-- toggle colorschemes
-- table list with colorschemes

local colors={'catppuccin_frappe', 'catppuccin_latte', 'catppuccin_macchiato', 'catppuccin_mocha',
            'dracula', 'everforest', 'everforest_high', 'everforest_alt', 'flatland',
            'gruvbox_dark', 'nord', 'onedark', 'papercolor', 'papercolor_dark', 'papercolor_light',
            'papercolor_serpia', 'selenized_black', 'solarized_dark', 'solarized_dark_high', 'ubuntu'}

local color_aux = 0
-- toggle colors up
function toggle_colors_up(bp)
    color_aux = color_aux + 1
    if color_aux > #colors then
        color_aux = 1
    end
    if color_aux <= #colors then
        config.SetGlobalOptionNative("colorscheme", colors[color_aux])
        micro.InfoBar():Message('(' .. color_aux .. ')' .. ' ' .. colors[color_aux])
    end
end

-- toggle colors down
function toggle_colors_down(bp)
    color_aux = color_aux - 1
    if color_aux < 1 then
        color_aux = #colors
    end
    if color_aux <= #colors then
        config.SetGlobalOptionNative("colorscheme", colors[color_aux])
        micro.InfoBar():Message('(' .. color_aux .. ')' .. ' ' .. colors[color_aux])
    end
end


-- toggle colorscheme between light and dark themes
function toggle_colorscheme()
    local cs = config.GetGlobalOption("colorscheme")

    if cs == light_mode then
        config.SetGlobalOptionNative("colorscheme", dark_mode)
        micro.InfoBar():Message(dark_mode)
    else
        config.SetGlobalOptionNative("colorscheme", light_mode)
        micro.InfoBar():Message(light_mode)
    end
end


-- setContains
function setContains(set, key)
    return set[key] ~= nil
end


-- output function
function output(bp)
    micro.Log("init.lua script output")
    bp:Save()
    local buf = bp.Buf
    local file_type = buf:FileType()
    local output_file = buf:GetName():gsub(".md$",".pdf")
    local so_file = buf:GetName():gsub(".c$",".so")
    local rust_file = buf.Path:gsub(".rs$","")
    local container_cmd = "distrobox enter ubuntu-dev -- "
    -- local gcc_lua = "gcc -std=c99 -pedantic -D_XOPEN_SOURCE=700 -Wall -Wextra -Wno-unused-parameter -fPIC -shared"
    -- local gcc_lua = "gcc -shared -fPIC -llua"

    _command = {}
    _command["c"] = "make module"
    _command["go"] = "go run " .. buf.Path
    _command["julia"] = "toolbox run -c dev julia " .. buf.Path
    _command["lua"] = home_dir .. "/.local/bin/lua " .. buf.Path
    _command["makefile"] = "setsid kitty --class kitty-compile " .. container_cmd .. "make"
    _command["markdown"] = "toolbox run -c dev pandoc -d " .. home_dir .. "/texmf/tex/pandoc/default.yaml -o " .. output_file .. " " .. buf.Path
    _command["python"] = "python " .. buf.Path
    _command["quarto"] = home_dir .. "/.local/bin/compile " .. buf.Path
    _command["r"] = "R CMD BATCH --vanilla --no-timing " .. buf.Path .. " /dev/null"
    _command["ruby"] = "ruby " .. buf.Path
    _command["shell"] = "bash " .. buf.Path
    _command["spice"] = "toolbox run -c dev ngspice -b " .. buf.Path
    _command["tex"] = bp
    _command["typst"] = home_dir .. "/.local/bin/compile " .. buf.Path

    -- call run_action
    -- first false means no bottom panel
    -- second false means no side pane with output
    run_action(bp.Buf, _command, "Output", false, false)
end


-- run_action
function run_action(buf, commands, identifier, bottom_panel, side_pane)
    micro.Log("init.lua run_action" .. identifier)

    local filetype = buf:FileType()

    if not setContains(commands, filetype) then
        -- if filetype does not support action just return
        return
    end

    if filetype == "tex" then
        local bp = commands[filetype]
        bp:HandleCommand("compiletex")
    else
        local output, err = shell.RunCommand(commands[filetype])
        local msg = output

        if err ~= nil then
            msg = msg .. tostring(err)
        end

        if msg ~= "" then
            if bottom_panel then
                local new_buffer = buffer.NewBuffer(msg, '')
                micro.CurPane():HSplitIndex(new_buffer, true)
            else
                -- show output command in a shell view
                shell.RunInteractiveShell('clear', false, false)

                -- Output message
                if side_pane then
                    micro.CurPane():VSplitIndex(buffer.NewBuffer(msg, ""), true)
                else
                    micro.TermMessage(msg)
                end
            end
        else
           micro.TermMessage(msg)
           micro.InfoBar():Message(identifier .. ": all good :)")
        end
    end
end

-- change settings
-- vertical split
function vsplit_left(bp)
    micro.Log("init.lua vsplit_left")
    micro.CurPane():VSplitIndex(buffer.NewBuffer("", ""), true)
end
-- horizontal split
function hsplit_down(bp)
    micro.Log("init.lua hsplit_down")
    micro.CurPane():HSplitIndex(buffer.NewBuffer("", ""), true)
end
-- toggle wrap
function toggle_softwrap(bp)
    micro.Log("init.lua toggle_softwrap")
    bp.Buf.Settings["softwrap"] = not bp.Buf.Settings["softwrap"]
end
-- toggle relative number
function toggle_relative(bp)
    micro.Log("init.lua toggle_relative")
    bp.Buf.Settings["relativeruler"] = not bp.Buf.Settings["relativeruler"]
end
-- toggle margin column
function toggle_column(bp)
    micro.Log("init.lua toggle_column")
    local aux = bp.Buf.Settings["colorcolumn"]
    if aux == 0 then
      bp.Buf.Settings["colorcolumn"] = 80
    else
      bp.Buf.Settings["colorcolumn"] = 0
    end
end
-- open a new view of current file in vertical split
function new_view(bp)
    micro.Log("init.lua new_view")
    micro.CurPane():VSplitIndex(bp.Buf, true)
    micro.InfoBar():Message("New View same file")
end


-- organize file
function tidy_file(bp)
    micro.Log("init.lua tidy_file")
    bp:SelectAll()
    bp:HandleCommand("replaceall '(^:.*);(.*)' '$2'")
    bp:HandleCommand("replaceall ' *$' ''")
    bp:HandleCommand("replaceall '.* \\$' ''")
    bp:HandleCommand("replaceall '(^ +)(.*$)' '$2'")
    bp:HandleCommand("replaceall '( +)' ' '")
    bp:HandleCommand("replaceall '^[;|\[|\\\\|\.|\\/|~|-](.*)' ''")
    bp:HandleCommand("replaceall '^\\b(cat|cd|curl|discogs|echo|ls|micro|rm)\\b(.*)' ''")
    bp:HandleCommand("replaceall '^\\b(touch|vi|vim|which|whois|xxd|yt|youtube)\\b(.*)' ''")
    bp:HandleCommand("textfilter sort")
    bp:SelectAll()
    bp:HandleCommand("textfilter uniq")
    bp:CursorStart()
    bp:Save()
end


-- sort and uniq
function sort_uniq(bp)
    micro.Log("init.lua sort_uniq")
    bp:SelectAll()
    bp:HandleCommand("replaceall ' *$' ''")
    bp:HandleCommand("replaceall '(^ +)(.*$)' '$2'")
    bp:HandleCommand("replaceall '( +)' ' '")
    bp:SelectAll()
    bp:HandleCommand("textfilter sort")
    bp:SelectAll()
    bp:HandleCommand("textfilter uniq")
    bp:CursorStart()
    bp:Save()
end


-- compile from makefile
function makefile_compile(bp)
    bp:Save()

    local cmd = "run setsid kitty --class kitty-compile distrobox enter ubuntu-dev -- make"
    bp:HandleCommand(cmd)

    msg = "Compiling Makefile..."
    micro.InfoBar():Message(msg)
end


-- tests
function _MainTest(bp)
    bp:HandleCommand("tab")
    -- bp:HandleCommand("replaceall '^\"(track\\d\\d)\"(.*)' '$1$2'")
    -- bp:HandleCommand("replaceall '\\t' ' '")
    -- bp:HandleCommand("replaceall '^(\\d\\d?\\d?)\. (.*) \\d:.*' 'track$1 = \"$2\"'")
end

function makeJobExit(out, args)
  local out = string.sub(out, -79)
  out = string.gsub(out, "\n", " ")
end


function make(bp)
    local buf = bp.Buf
    local bn = bp.Buf.AbsPath
    local pdf_file = buf:GetName():gsub(".tex$",".pdf")

    local filePath = "~/Downloads/README.md"

    -- local nb = buffer.NewBufferFromFile(filePath)

    -- micro.CurPane():NewTabCmd({filePath})

    n = config.FileComplete()

    -- isSameBuffer = (bn == bp)

    micro.TermMessage(n)
    -- micro.CurPane():VSplitIndex(buffer.NewBuffer(msg, ""), true)
    -- micro.InfoBar():Message(isSameBuffer)

-- bp:Save()
-- shell.RunCommand('make')
-- shell.JobStart(home_dir .. "/.local/bin/open-pdf.sh  ".. pdf_file, nil, nil, makeJobExit, nil)
end


local ntest1 = 0
function onSetActive(bp)
    ntest1 = ntest1 + 1
    micro.InfoBar():Message("ok")
end

-- =============================================================================
-- Program:       fzf.lua
-- Description:   fzf find for micro editor
-- Software/Tool: lua(5.4.4)
-- =============================================================================

VERSION = "1.1.1"

local micro = import("micro")
local shell = import("micro/shell")
local config = import("micro/config")
local buffer = import("micro/buffer")
local path = import("path")


function fzf(bp)
    local ft = bp.Buf:FileType()
    local cmd = "fzf --tac --highlight-line \
        --layout=reverse --border=rounded --margin=10% \
        --padding=5% --info=right --no-scrollbar \
        --pointer='➜'"

    if shell.TermEmuSupported then
        local err = shell.RunTermEmulator(bp, cmd, false, true, fzfOutput, {bp})
        if err ~= nil then
            micro.InfoBar():Error(err)
        end
    else
        local output, err = shell.RunInteractiveShell(cmd, false, true)
        if err ~= nil then
            micro.InfoBar():Error(err)
        else
            fzfOutput(output, {bp})
        end
    end
end


function fzfOutput(output, args)
    local bp = args[1]
    local strings = import("strings")
    output = strings.TrimSpace(output)

    if output ~= "" then
        local buf, err = buffer.NewBufferFromFile(output)
        local cmd = string.format("tab %s", output)

        if err == nil then
            -- Original behavior, open it in same pane.
            -- bp:OpenBuffer(buf)

            -- Modification to open it in new tab.
            bp:HandleCommand(cmd)
        end
    end
end

-- Functions to export
function init()

    -- Commands
    config.MakeCommand("fzf", fzf, config.NoComplete)

    -- Binds to init.lua
    config.TryBindKey("Ctrl-o", "lua:fzf.fzf", false)

end

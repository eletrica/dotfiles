-- =============================================================================
-- Program:       microtex.lua
-- Description:   Compile and forward pdf made in LaTeX
-- Software/Tool: micro/lua/LaTeX
-- =============================================================================

-- https://github.com/zyedidia/micro/issues/1295
-- https://github.com/chykcha3/micro-plugin-latex

VERSION = "0.4.3"


local micro = import("micro")
local config = import("micro/config")
local shell = import("micro/shell")
local buffer = import("micro/buffer")
local util = import("micro/util")
local utf8 = import("unicode/utf8")


-- Message test
function testHandler(text)
    micro.InfoBar():Message(text)
end


-- Verify if file exists
function file_exists(name)
   local f = io.open(name, "r")

   return f ~= nil and io.close(f)
end


-- Sync to open pdf file in page of current content in document
function synctexForward(bp)

    local syncDoc = "documento.synctex"

    if file_exists(syncDoc) then
        local fileName = bp.Buf:GetName():gsub(" ", "\\%0")

        isTexMainFile = (fileName == "documento.tex")

        if isTexMainFile then
            local pdfFileName = "documento.pdf"
            local cursor = bp.Buf:GetActiveCursor()
            local cursor_location = cursor.Y + 1
            local zathuraArgPos = string.format(" --synctex-forward=%i:%i:%s", cursor_location, cursor.X, fileName)
            local zathuraArgFile = " " .. pdfFileName;

            micro.InfoBar():Message("Forward from line " .. cursor_location .. " in " .. fileName)

            shell.JobStart("zathura " .. zathuraArgPos .. zathuraArgFile, nil, nil, dummyFunc)
        else
            local pdfFileName = "documento.pdf"

            -- Get file path from main directory
            local fileName = bp.Buf.Path:gsub(".tex$","") .. ".tex"

            local cursor = bp.Buf:GetActiveCursor()
            local cursor_location = cursor.Y + 1
            local zathuraArgPos = string.format(" --synctex-forward=%i:%i:%s", cursor_location, cursor.X, fileName)
            local zathuraArgFile = " " .. pdfFileName;

            micro.InfoBar():Message("Forward from line " ..cursor_location .. " in " .. fileName)

            shell.JobStart("zathura " .. zathuraArgPos .. zathuraArgFile, nil, nil, dummyFunc)
        end
    else
        micro.InfoBar():Message(syncDoc .. " not exists")
    end
end


function synctexBackward(pos)
    local bp = micro.CurPane()

    bp:GotoCmd({pos:sub(1, -2)})
end


function lint(bp)
    local fileName = bp.Buf:GetName()

    local output = shell.ExecCommand("distrobox enter ubuntu-dev -- pdflatex", "-synctex=15", "-interaction=nonstopmode", "-draftmode", "-file-line-error", fileName)
    local error = output:match("[^\n/]+:%w+:[^\n]+")

    if error then
        micro.InfoBar():Message(error)
        local errorPos = error:match(":%w+:"):sub(2, -2)
        micro.CurPane():GotoCmd({errorPos})
        return true
    else
        return false
    end
end


-- Compile from Makefile
function compile(bp)
    local fileName = "documento"
    local cmd = "run distrobox enter ubuntu-dev -- make sync"

    bp:Save()

    synctexForward(bp)

    bp:HandleCommand(cmd)
    micro.InfoBar():Message("Compiling document...")
end


function dummyFunc()

end


-- Do when open a file
function onBufferOpen(buf)
    isTex = (buf:FileType() == "tex")

    if isTex then
        local fileName = 'documento.tex'
        local syncFileNameN = "/tmp/" .. fileName .. ".fifo"
        local syncFileName = syncFileNameN:gsub(" ", "\\%0")
        local scriptFifoWriteFileName = "/tmp/" .. fileName .. ".fifo-writer.sh"
        local scriptFifoWrite = "echo \"$@\" > " .. syncFileName
        local scriptFifoRead = "while true; do if read line; then echo $line; fi; sleep 2; done < " .. syncFileName

        shell.ExecCommand("if [ -p" .. syncFileName .. "]; then; unlink" .. syncFileName .. "; fi; mkfifo", syncFileName)

        local f = io.open(scriptFifoWriteFileName, "w")
        f:write(scriptFifoWrite)
        f:close()

        shell.ExecCommand("chmod", "755", scriptFifoWriteFileName)

        jobFifoRead = shell.JobStart(scriptFifoRead, synctexBackward, nil, dummyFunc)
    end
end


function preSave(bp)
    if isTex then
        isBufferModified = bp.Buf:Modified()
    end
end


function preQuit(bp)
    if isTex then
        local fileName = 'documento.tex'
        local syncFileName = fileName .. ".temp"
        local scriptFifoWriteFileName = fileName .. ".fifo-writer.sh"

        shell.JobStop(jobFifoRead)
        shell.ExecCommand("rm", syncFileName)
        shell.ExecCommand("rm", scriptFifoWriteFileName)
    end
end


-- Functions to export
function init()

    -- Commands
    config.AddRuntimeFile("microtex-plugin-help", config.RTHelp, "help/microtex-plugin.md")
    config.MakeCommand("compiletex", compile, config.NoComplete)
    config.MakeCommand("synctex-forward", synctexForward, config.NoComplete)

    -- Binds to init.lua
    config.TryBindKey("F5", "lua:microtex.compile", false)
    config.TryBindKey("F6", "lua:microtex.synctexForward", false)
end

" Vimscript file settings ------------------------------------------------- {{{

augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END

autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritePre *.Xresources !xrdb %

" -------------------------------------------------------------------------- }}}

" =============================================================================
" custom functions
" =============================================================================

" open buffers in fzf search
function! s:buflist()
  redir => ls
  silent ls
  redir END
  return split(ls, '\n')
endfunction

function! s:bufopen(e)
  execute 'buffer' matchstr(a:e, '^[ 0-9]*')
endfunction

" =============================================================================
" map keys in normal mode
" =============================================================================

" leader key
let mapleader="\<space>"
" localleader key
let maplocalleader="\\"

" toggle NERDTree
nmap <c-r> :NERDTreeToggle<CR>

" copy selection for clipboard in wayland
nnoremap <leader>c :r! $(wl-copy -p)<cr>

" open current buffers in fzf
nnoremap <silent> <Leader><Enter> :call fzf#run({
\   'source':  reverse(<sid>buflist()),
\   'sink':    function('<sid>bufopen'),
\   'options': '+m',
\   'down':    len(<sid>buflist()) + 2
\ })<CR>

" open vim config files in new split
nnoremap <leader>em :vs $HOME/.vim/init/mappings.vim<cr>
nnoremap <leader>ep :vs $HOME/.vim/init/plugins.vim<cr>
nnoremap <leader>es :vs $HOME/.vim/init/settings.vim<cr>
nnoremap <leader>et :vs $HOME/.vim/init/theme.vim<cr>

nnoremap <leader>f :filetype detect<cr>

" open horizontal split
nnoremap <leader>h :split<cr>

" toggle text wrap
nnoremap <leader>p :set wrap!<cr>

" close current buffer
noremap <leader>q :bd<cr>

nnoremap <leader>s :source $MYVIMRC<cr>
nnoremap <leader>t :set list!<cr>
nnoremap <leader>u :<up><cr>
nnoremap <leader>v :vsplit<cr>
nnoremap <leader>w :w<cr>
nnoremap <leader>x :<up><cr>

" buffers navigation
nnoremap <silent><left> :bprevious<cr>
nnoremap <silent><right> :bnext<cr>

" center visualizations with navigation keys
nnoremap <up> 25kzz
nnoremap <down> 25jzz
noremap G Gzz
noremap n nzz
noremap N Nzz
noremap { {zz
noremap } }zz

""" moving between files """
nnoremap <silent> <A-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <A-Right> :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>

""" Moving between splits """
nnoremap <c-h> <c-w>h<cr>
nnoremap <c-j> <c-w>j<cr>
nnoremap <c-k> <c-w>k<cr>
nnoremap <c-l> <c-w>l<cr>

""" resize splits """
nnoremap <c-left> <c-w><<cr>
nnoremap <c-right> <c-w>><cr>
nnoremap <c-up> <c-w>+<cr>
nnoremap <c-down> <c-w>-<cr>

" fzf search from current dir
nnoremap <c-p> :Files<cr>

" word search with silversearch
nnoremap <c-f> :Ag<space>

" modify files
nnoremap <c-y> :redo<cr>

nnoremap <localleader>d ddO<esc>
nnoremap <localleader>c :term compile %:p<cr>
nnoremap <localleader>q :q!<cr>
nnoremap <localleader>t :echo &ft<cr>

" clear extra spaces
nnoremap <localleader>s :%s/\s\+$//<cr>:let @/=''<cr>

" compile tex file in vimtex
noremap <F5> :VimtexCompile<cr>

" see vimtex compile output
noremap <F6> :VimtexView<cr>

" =============================================================================
" map keys in insert mode
" =============================================================================

" emmet plugin
imap <expr> <tab> emmet#expandAbbrIntelligent("\<tab>")

" enter in normal mode when in insert mode
inoremap jk <esc>

" =============================================================================
" map keys in visal mode
" =============================================================================

" copy selected lines in wayland
vmap <silent> y y:silent call system("wl-copy", getreg("\""))<CR>

" descomment lines in visual mode
vnoremap <c-k> <c-v>ld <esc>

" =============================================================================
" line comments group
" =============================================================================

augroup comments_files
  autocmd!
  " Single
  autocmd BufNewFile,BufRead *.vim,*vimrc nnoremap <leader>/ I" <esc>
  autocmd BufNewFile,BufRead *.tex nnoremap <leader>/ I% <esc>
  autocmd BufNewFile,BufRead *.py,*.rb nnoremap <leader>/ I# <esc>
  " Multiple
  autocmd BufNewFile,BufRead *.py,*.rb vnoremap <leader>/ <c-v>I# <esc>
  autocmd BufNewFile,BufRead *.py,*.rb vnoremap <c-l> <c-v>d <esc>
  autocmd BufNewFile,BufRead *.tex vnoremap <leader>/ <c-v>I% <esc>
  autocmd BufNewFile,BufRead *.c,*.js vnoremap <leader>/ <c-v>I// <esc>
augroup END

" =============================================================================
" latin chars group
" =============================================================================

augroup latin_1
  autocmd!
  autocmd BufNewFile,BufRead *.md,*.csv,*.txt,*.html inoremap 'a á
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap 'e é
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap 'i í
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap 'o ó
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap 'u ú
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap `a à
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap ~a ã
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap ~o õ
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap ^a â
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap ^e ê
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap ^o ô
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap 'c ç
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap 'A Á
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap 'E É
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap 'I Í
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap 'O Ó
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap 'U Ú
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap `A À
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap ~A Ã
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap ~O Õ
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap ^A Â
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap ^E Ê
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap ^O Ô
  autocmd BufNewFile,BufRead *.csv,*.txt,*.html inoremap 'C Ç
augroup END

" =============================================================================
" markdown group
" =============================================================================

augroup markdown_file
  autocmd!
  autocmd BufNewFile,BufRead *.md nnoremap ;c O```c<esc>jo<esc>i```<esc>
  autocmd BufNewFile,BufRead *.md nnoremap ;db O```sql<esc>jo<esc>i```<esc>
  autocmd BufNewFile,BufRead *.md nnoremap ;py O```python<esc>jo<esc>i```<esc>
  autocmd BufNewFile,BufRead *.md nnoremap ;rb O```ruby<esc>jo<esc>i```<esc>
  autocmd BufNewFile,BufRead *.md nnoremap ;sh O```bash<esc>jo<esc>i```<esc>
  autocmd BufNewFile,BufRead *.md nnoremap ;tex O```tex<esc>jo<esc>i```<esc>
augroup END

" =============================================================================
" TeX group
" =============================================================================

augroup latex_file
  autocmd!
  autocmd FileType tex,latex inoremap <c-k> \textit{}<esc>i
  autocmd FileType tex,latex inoremap <c-b> \textbf{}<esc>i
  autocmd FileType tex,latex inoremap <c-d> \dfrac{}{}<esc>2hi
  autocmd FileType tex,latex inoremap <c-s> _{}<esc>hci{
"  autocmd FileType tex,latex inoremap <c-u> ^{}<esc>hci{
  autocmd FileType tex,latex inoremap <c-r> \sqrt{}<esc>i
  autocmd FileType tex,latex inoremap <c-e> $$  $$<esc>2hi
  autocmd FileType tex,latex inoremap 'a \'a
  autocmd FileType tex,latex inoremap 'e \'e
  autocmd FileType tex,latex inoremap 'i \'i
  autocmd FileType tex,latex inoremap 'o \'o
  autocmd FileType tex,latex inoremap 'u \'u
  autocmd FileType tex,latex inoremap `a \`a
  autocmd FileType tex,latex inoremap ~a \~a
  autocmd FileType tex,latex inoremap ~o \~o
  autocmd FileType tex,latex inoremap ^a \^a
  autocmd FileType tex,latex inoremap ^e \^e
  autocmd FileType tex,latex inoremap ^o \^o
  autocmd FileType tex,latex inoremap 'c \c{c}
  autocmd FileType tex,latex inoremap 'A \'a
  autocmd FileType tex,latex inoremap 'E \'E
  autocmd FileType tex,latex inoremap 'I \'I
  autocmd FileType tex,latex inoremap 'O \'O
  autocmd FileType tex,latex inoremap 'U \'U
  autocmd FileType tex,latex inoremap `A \`A
  autocmd FileType tex,latex inoremap ~A \~A
  autocmd FileType tex,latex inoremap ~O \~O
  autocmd FileType tex,latex inoremap ^A \^A
  autocmd FileType tex,latex inoremap ^E \^E
  autocmd FileType tex,latex inoremap ^O \^O
  autocmd FileType tex,latex inoremap 'C \c{C}
  autocmd FileType tex,latex inoremap ;doc \begin{document}<cr>\end{document}<esc>Vk3<o<space>
  "autocmd FileType tex,latex nnoremap ;cc :VimtexCompile<cr>
augroup END

" =============================================================================
" R group
" =============================================================================

augroup rfiles
  autocmd!
  autocmd BufNewFile,BufRead *.rmd inoremap ;r <esc>o<esc>i```{r}<esc>o```<esc>O
  autocmd BufNewFile,BufRead *.rmd inoremap ;c O```{c}<esc>jo<esc>i```<esc>
  autocmd BufNewFile,BufRead *.rmd inoremap ;py O```{python}<esc>jo<esc>i```<esc>
  autocmd BufNewFile,BufRead *.rmd inoremap ;rb O```{ruby}<esc>jo<esc>i```<esc>
  autocmd BufNewFile,BufRead *.rmd inoremap ;sh O```{bash}<esc>jo<esc>i```<esc>
augroup END

" =============================================================================
" ruby and elixir frameworks like group
" =============================================================================

augroup framework_file
  autocmd!
  autocmd FileType erb,eruby,eelixir inoremap ;5 <esc>^i<% <esc>A %><esc>o
  autocmd FileType erb,eruby,eelixir inoremap ;= <esc>^i<%= <esc>A %><esc>o
augroup END

" =============================================================================
" sessions group
" =============================================================================

" multiple sessions
noremap <localleader>1 :mksession! ~/.vim/sessions/session_1.vim<cr>
noremap <leader>1 :source ~/.vim/sessions/session_1.vim<cr>

noremap <localleader>2 :mksession! ~/.vim/sessions/session_2.vim<cr>
noremap <leader>2 :source ~/.vim/sessions/session_2.vim<cr>

noremap <localleader>3 :mksession! ~/.vim/sessions/session_3.vim<cr>
noremap <leader>3 :source ~/.vim/sessions/session_3.vim<cr>

noremap <localleader>4 :mksession! ~/.vim/sessions/session_4.vim<cr>
noremap <leader>4 :source ~/.vim/sessions/session_4.vim<cr>

" automatically save the current session whenever vim is closed
autocmd VimLeave * mksession! ~/.vim/sessions/shutdown_session.vim
noremap <F7> :source ~/.vim/sessions/shutdown_session.vim<CR>

" If you really want to, this next line should restore the shutdown session
" automatically, whenever you start vim.  (Commented out for now, in case
" somebody just copy/pastes this whole block)
"
" autocmd VimEnter source ~/.vim/shutdown_session.vim<CR>

" =============================================================================
" custom macros
" =============================================================================

" press ctrl-v before type a special button like <esc>.
" <esc> = ctrl-[
" <cr> = ctrl-m
" let @x='0O[[tracks]]j0itracknumber = "f-hxxxi"artist = "f-hxxs"title = "$xxxs"kkkddjpYpdt iimagelllllicovers/cover$i.jpg0jj0'
let @x='yypokkko[[tracks]]0jxxxxxxstitlej0llllanumber = "f C"0jj0'
let @c='0itrackf-xs= "$xxxs"j0'
let @e='0ggjjjddpjddkkkp0ggogsid = ""mbid = ""oformat = "File"jjostyle = "Video Game"0ggjjjddjjpkddjjjp'

" =============================================================================
" temp section, not used, but keep for check
" =============================================================================

" noremap 9 :tabprevious<cr> "noremap 0 :tabnext<cr>
" noremap <c-i> :bnext<cr>
" noremap <c-u> :bprevious<cr>
" nnoremap <leader>c :r! $(echo xclip -o)<cr>
" vmap <silent> c y :silent call system("xclip -r -selection clipboard", getreg("\""))<CR>
" vmap <C-c> "+yi
" vmap <C-v> c<ESC>"+p
" vmap <C-x> "+c
" imap <C-v> <ESC>"+pa

" help vimtex in inverse pff search
"function! s:write_server_name() abort
"  let nvim_server_file = (has('win32') ? $TEMP : '/tmp') . '/vimtexserver.txt'
"  call writefile([v:servername], nvim_server_file)
"endfunction

"augroup vimtex_common
"  autocmd!
"  autocmd FileType tex call s:write_server_name()
"augroup END

" Compile
"autocmd FileType python nnoremap \c :! clear & python %:p<cr>

" autocmd FileType tex,latex nnoremap;
" \ cc :VimtexCompile && rm *.aux *.bbl *.log *.nav *.out *.snm *.toc && rm *.gz &&
" \ clear<cr>
" autocmd FileType tex,latex nnoremap;
" \ lc :!pdflatex -synctex=1 -interaction=nonstopmode -aux-directory=/tmp % &&
" \ zathura *.pdf && rm *.gz && clear<cr>

" manually save a session with <F5>
" noremap <F5> :mksession! ~/.vim/sessions/manual_session.vim<cr>
" recall the manually saved session with <F6>
" noremap <F6> :source ~/.vim/sessions/manual_session.vim<cr>

" <F7> restores that 'shutdown session'


" airline settings --------------------------------------------------------- {{{

" bar colorschems
" let g:airline_theme='hybrid'
" let g:airline_theme='luna'
" let g:airline_theme='angr'
" let g:airline_theme='jellybeans'
" let g:airline_theme='papercolor'

let g:airline#extensions#fugitiveline#enabled=0
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#left_sep=' '
let g:airline#extensions#tabline#left_alt_sep='|'
let g:airline#extensions#tabline#formatter='default'
let g:airline_base16_monotone=1
let g:airline_powerline_fonts=1
let g:airline_detect_modified=1
let g:airline_detect_paste=1
let g:airline_detect_crypt=1
let g:airline_detect_spell=1
let g:airline_detect_spelllang=1
let g:airline_inactive_alt_sep=1
let g:airline_detect_iminsert=0
let g:airline_inactive_collapse=1
let g:ctrlp_working_path_mode='r'
let g:netrw_liststyle=3
let g:netrw_altv=1
" -------------------------------------------------------------------------- }}}

" gruvbox settings --------------------------------------------------------- {{{
set background=dark
let g:gruvbox_contrast_dark='hard'
let g:gruvbox_italicize_comments=1
let g:gruvbox_termcolors=256
silent colorscheme gruvbox
" -------------------------------------------------------------------------- }}}

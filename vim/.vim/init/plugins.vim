" Emmet
let g:user_emmet_leader_key=','
" let g:user_emmet_settings = webapi#json#decode(join(readfile(expand('~/.snippets_custom.json')), "\n"))

" Multiple Cursors
let g:multi_cursor_use_default_mapping=1
" let g:multi_cursor_start_word_key      = '<C-p>'
" let g:multi_cursor_select_all_word_key = '<A-n>'
" let g:multi_cursor_start_key           = 'g<C-n>'
" let g:multi_cursor_select_all_key      = 'g<A-n>'
" let g:multi_cursor_next_key            = '<A-y>'
" let g:multi_cursor_prev_key            = '<A-p>'
" let g:multi_cursor_skip_key            = '<A-x>'
" let g:multi_cursor_quit_key            = '<Esc>'

" NERDTree
" autocmd vimenter * NERDTree
" let NERDTreeQuitOnOpen = 1
" let g:NERDTreeHijackNetrw = 0
let g:netrw_dirhistmax=0

" Polyglot
" let g:polyglot_disabled = ['latex']

" Syntatisc
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_tex_lacheck_quiet_messages = { 'regex': '\Vpossible unwanted space at'}

" Vimtex
" autocmd BufReadPre *.tex
" let b:vimtex_main = 'main.tex'
" let b:suppress_latex_suite = 1
" let g:vimtex_view_general_options='-reuse-instance -forward-search @tex @line @pdf'
" let g:tex_flavor = 'latex'
" let g:vimtex_compiler = ''
" let g:vimtex_view_method = 'zathura'
" let g:vimtex_view_general_viewer = 'zathura'
" let g:vimtex_latexmk_options = '-pdf -pdflatex="pdflatex --shell-escape %O %S" -verbose -file-line-error -synctex=1 -interaction=nonstopmode'

" let g:tex_conceal='abdmg'
" let g:tex_fold_enabled=1
" let g:vimtex_quickfix_mode = 0
" let g:vimtex_view_automatic = 0
" let g:vimtex_view_general_options_latexmk = '-r'
" let g:vimtex_view_general_options_latexmk='-reuse-instance'
" set conceallevel=1
" set iskeyword+=:

" compiling using makefile
let g:vimtex_compiler_method = 'generic'
let g:vimtex_compiler_generic = {'command': 'make distrobox'}

let g:Tex_DefaultTargetFormat='pdf'
let g:vimtex_view_enabled=1
let g:vimtex_view_automatic=1
let g:vimtex_view_general_viewer = 'zathura'
let g:vimtex_view_method = 'zathura'
let g:vimtex_compiler_progname = 'nvr'
let g:tex_flavor = "latex"

" let b:did_ftplugin = 1

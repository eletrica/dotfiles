autocmd GUIEnter * set visualbell t_vb=
set noerrorbells visualbell t_vb=

filetype plugin on
filetype indent on
syntax enable


let g:skip_defaults_vim = 1
let g:tex_no_error=1

retab
set autoindent expandtab tabstop=2 softtabstop=2 shiftwidth=2
set autochdir
set autowrite
set bs=2
set clipboard=unnamed
set cursorline
set encoding=utf-8
set foldmethod=marker
set foldmarker=<<<,>>>
set foldclose=all
set hidden
set hlsearch
set ignorecase
set incsearch
set laststatus=2
set linebreak
set listchars=tab:→\ ,space:·,nbsp:␣,trail:•,eol:¶,precedes:«,extends:»
set mouse=v
set nocompatible
set nolist
set nospell
set noswapfile
set number
set paste
" set relativenumber
set shiftwidth=4
set showbreak=/n
set showcmd
set showmatch
set showmode
set smartcase
set splitbelow
set tabstop=4
" set termwinsize=10x300
set t_Co=256
set vb
set viminfofile=NONE
set wildmenu
set wildmode=list:longest,longest:full
set wrap
set wrapmargin=20

"Font running in gui mode
if has('gui_running')
   " set go=agimrLtT
  set go=
  set guifont=JetBrainsMono\ Nerd\ Font\ 16
endif
